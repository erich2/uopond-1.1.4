//
//  profileViewController.swift
//  uoPond
//
//  Created by Ethan Richards on 4/19/19.
//  Copyright © 2019 Ethan Richards. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseStorage
import FirebaseAuth

class profileViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var firstNameBox: UITextField!
    @IBOutlet weak var lastNameBox: UITextField!
    @IBOutlet weak var doneButton: UIButton!
    @IBOutlet weak var uploadButton: UIButton!
    @IBOutlet weak var nextButton: UIButton!
    var UID:String?
    @IBOutlet weak var button: UIButton!
    var email:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // THE ERRROR NEED TO UID print("THE UID: ",self.userUid!)
        self.doneButton.layer.cornerRadius = 10
        self.profilePic.image = #imageLiteral(resourceName: "Default-welcomer")
        self.profilePic.layer.cornerRadius = self.profilePic.frame.size.width / 2
        self.profilePic.clipsToBounds = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapped(sender:)))
        self.view.addGestureRecognizer(tap)
        self.roundCorners(view: self.uploadButton, corners: [.topLeft, .bottomRight], radius: 10)
        self.firstNameBox.addBottomBorder(UIColor.black, height: 1)
        self.roundCorners(view: self.firstNameBox, corners: [.topLeft, .topLeft], radius: 10)
        self.roundCorners(view: self.lastNameBox, corners: [.bottomLeft, .bottomRight], radius: 10)
        self.firstNameBox.attributedPlaceholder = NSAttributedString(string: "first name", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        self.lastNameBox.attributedPlaceholder = NSAttributedString(string: "last name", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
    }

    @IBAction func endEditing(_ sender: Any) {
        self.view.endEditing(true)
    }
    
    
    
    
    func roundCorners(view :UIView, corners: UIRectCorner, radius: CGFloat){
            let path = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            view.layer.mask = mask
    }
       
    
    @IBAction func uploadPressed(_ sender: Any) {
        let picker = UIImagePickerController()
        picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        picker.allowsEditing = true
        picker.delegate = self
        present(picker, animated: true, completion: nil)
    }
    
    @objc func tapped(sender: UIGestureRecognizer) {
           self.view.endEditing(true)
           print("tapped")
       }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        var selectedImage: UIImage?
        if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage{
            selectedImage = editedImage
        }else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
            selectedImage = originalImage
        }
        self.profilePic.image = selectedImage
        
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
    }
    

    
    @IBAction func button(_ sender: Any) {
        let vc = self.storyboard!.instantiateViewController(withIdentifier: "homeSB") as! homeViewController
        vc.userUid = self.UID!
        vc.welcomeUser()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    
    @IBAction func donePressed(_ sender: Any) {
   
        if self.lastNameBox.text == "" || self.firstNameBox.text == "" || self.profilePic == UIImage(named: "Default-welcomer") {
            
            let alertController = UIAlertController(title: "Oops", message: "Please fill out all fields", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
           
        }else{
            print("right before animating")
            self.showSpinner(onView: self.view)
            guard let image = self.profilePic.image else{
                print("error")
                return
            }
            //let imageName = UUID().uuidString
            guard let uid = self.UID else{
                print("error on uid")
                return
            }
            print("before getting image data")
            let data = UIImageJPEGRepresentation(image, 1.0)
            let imageRef = Storage.storage().reference().child("users").child(self.UID!).child("profPicture.jpg")
            imageRef.putData(data!, metadata: nil) { (metadata, error) in
                if error != nil {
                    print("error in put data")
                    print(error!)
                    return
                }else{
                    print("no error in put data")
                    imageRef.downloadURL(completion: { (url, err) in
                        if err != nil{
                            print(err!)
                            return
                        }else{
                            print("referring to firebase")
                            self.saveData(url: url!, uid: uid) {
                                self.performSegue(withIdentifier: "user2Home", sender: self)
                                self.removeSpinner()
                            }
                            
                        }
                        print("AFTER VALUES SHOULD HAVE UPDATED")
                    })
                }
             //   self.removeSpinner()
            }
            self.doneButton.isHidden = true
            print("remove spinner")
            
           
//                self.removeSpinner()
//                let vc = self.storyboard!.instantiateViewController(withIdentifier: "homeSB") as! homeViewController
//                vc.userUid = self.UID!
//                vc.welcomeUser()
//                self.navigationController?.pushViewController(vc, animated: true)

            
        }
    }
    
    func saveData(url:URL, uid:String, completion : () -> Void){
        let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/")
        let userReference = ref.child("Users").child(uid)
        let values = ["firstName" : self.firstNameBox.text!, "lastName" : self.lastNameBox.text!, "profilePicture": String(describing: url)]
        print("BEFORE UPDATING VALUES")
        userReference.updateChildValues(values, withCompletionBlock: { (err, ref) in
            if err != nil{
                print(err!)
                return
            }else{
                print("success")
            }
        })
        completion()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "user2Home"{
            let vc = segue.destination as! UINavigationController
            let target = vc.topViewController as! homeViewController
            target.userUid = self.UID!
            target.welcomeUser()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
}

