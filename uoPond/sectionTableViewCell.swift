//
//  sectionTableViewCell.swift
//  uoPond
//
//  Created by Ethan Richards on 2/4/20.
//  Copyright © 2020 Ethan Richards. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseStorage
import FirebaseAuth

class sectionTableViewCell: UITableViewCell, UICollectionViewDelegate, UICollectionViewDataSource {
    

    @IBOutlet weak var collectionViewRight: NSLayoutConstraint!
    @IBOutlet weak var maximizeButton: UIButton!
    @IBOutlet weak var totalItems: UILabel!
    @IBOutlet weak var categoryEmote: UIImageView!
    @IBOutlet weak var labelView: UIView!
    @IBOutlet weak var collectionView: UICollectionView!
    var posts = [sectionPost]()
    @IBOutlet weak var categoryLabel: UILabel!
    var correctPosts = [sectionPost]()
    var blacklist = [String]()
    var vert:Bool = false
    var myID = Auth.auth().currentUser?.uid
    var postID = [String]()
    var userPins = [String]()
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.posts = []
        self.correctPosts = []
        collectionView.delegate = self
        collectionView.dataSource = self
        self.labelView.layer.cornerRadius = 10
//        collectionView.tag = row
        collectionView.setNeedsDisplay()
        setTheFlow()
        //DispatchQueue.global(qos: .background).async {
//            self.gatherPosts(completion: { () -> Void in
//                self.collectionView.reloadData()
//                self.organizePosts()
//                self.totalItems.text = "(\(self.correctPosts.count))"
//                //self.gatherPins()
//
//            })
        //}
        self.labelView.backgroundColor = UIColor.black
    }
    

    func setTheFlow(){
        if self.vert == false {
            let flowLayout = UICollectionViewFlowLayout()
            self.collectionViewRight.constant = 16
            flowLayout.itemSize = CGSize(width: 210, height: 210)
            flowLayout.scrollDirection = .horizontal
            flowLayout.minimumInteritemSpacing = 10.0
            flowLayout.sectionInset = .init(top: 10, left: 0, bottom: 0, right: 0)
            self.collectionView.collectionViewLayout = flowLayout
        }else{
            let flowLayout = UICollectionViewFlowLayout()
            self.collectionViewRight.constant = 8
            flowLayout.itemSize = CGSize(width: 90, height: 90)
            flowLayout.scrollDirection = .vertical
            flowLayout.minimumInteritemSpacing = 1.0
            flowLayout.minimumLineSpacing = 10
            flowLayout.sectionInset = .init(top: 0, left: 0, bottom: 0, right: 0)
            self.collectionView.collectionViewLayout = flowLayout
        }
        
        self.collectionView.setNeedsDisplay()
    }
   
    
      func gatherPosts(completion: @escaping () -> Void?){
            let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Posts")
            ref.observe(.value) { (snapshot) in
                if let snapshots = snapshot.children.allObjects as? [DataSnapshot]{
                    for child in snapshots{
                        let value = child.value as? NSDictionary
                        let category = value?["section"] as? String ?? ""
                        let downloadURL = value?["image0"] as? String ?? ""
                        let id = value?["user"] as? String ?? ""
                        let flagged = value?["flagged"] as? String ?? ""
                        let price = value?["price"] as? String ?? ""
                        let post = sectionPost(image: downloadURL, type: category, id: id, price: price)
                        
                        if (self.blacklist.contains(id)){
                            print("blocked user")
                        }else {
                            if (flagged == "yes"){
                                print("flagged post")
                            }else{
                                self.posts += [post]
                                self.postID.append(id)
                            }
                        }
                        
                    }
                    completion()
                    }
                else{
                    print("ERROR")
                }
                }
        }

    func organizePosts(){
        print(self.posts.count)
        for post in 0..<self.posts.count {
           
            if self.posts[post].type == self.categoryLabel.text! {
                self.correctPosts.append(self.posts[post])
                print("appended")
            }
        }
    }
    
    
    
    
//    func gatherPins(){
//        for id in 0..<self.postID.count{
//            let cat = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Posts")
//            cat.child("Post\(self.postID[id])").observe(.value) { (snapshot) in
//            if let snaps = snapshot.children.allObjects as? [DataSnapshot]{
//                for child in snaps{
//                    let value = child.value as? NSDictionary
//                    let name = value?["price"] as? String ?? ""
//                    print(name)
//                }
//                }
//            }
//        }
//    }
    
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    
    }
    
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        print(self.correctPosts.count)
        return self.correctPosts.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "collectionItem", for: indexPath) as! sectionCollectionViewCell
        // tap and hold
       
        print(self.correctPosts[indexPath.row].type)
        let text = UITextView(frame: CGRect(x: 0, y: 0, width: 100, height: 100))
        text.text = self.correctPosts[indexPath.row].type
        cell.addSubview(text)
        let downloadURL = self.correctPosts[indexPath.row].image
        let storageRef = Storage.storage().reference(forURL: downloadURL)
        storageRef.getData(maxSize: 20000000, completion: { (data, error) in
            if error != nil{
                print("FIND ME RIGHT HERE:", error!)
                return
            }else{
                let postImage = UIImage(data: data!)
                cell.imagePost.image = postImage
                
            }
        })
        
        return cell
    }
    
    @available(iOS 13.0, *)
    @IBAction func maximizePressed(_ sender: Any) {
        if self.vert == false {
            self.vert = true
            self.setTheFlow()
            self.contentView.sizeToFit()
            self.collectionView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: 200)
            self.maximizeButton.setImage(UIImage(systemName: "plus.circle.fill"), for: .normal)
        }else{
            self.vert = false
            self.setTheFlow()
            self.maximizeButton.setImage(UIImage(systemName: "minus.circle.fill"), for: .normal)
        }
        
    }
    
}
