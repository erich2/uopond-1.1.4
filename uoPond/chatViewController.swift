//
//  chatViewController.swift
//  uoPond
//
//  Created by Ethan Richards on 1/29/20.
//  Copyright © 2020 Ethan Richards. All rights reserved.
//

import UIKit
import Firebase
import FirebaseStorage
import FirebaseDatabase
import FirebaseAuth
import PassKit
import FirebaseFunctions
import FirebaseFirestore
import Braintree
import BraintreeDropIn


class chatViewController: UIViewController, UITextViewDelegate{
    
   
    
    lazy var functions = Functions.functions()
    var firstMess = [Message]()
    var finalMessages = [Message]()
    var receiverName:String?
    var myID:String?
    var myName:String?
    var receiverID:String?
    var offerTitle:String?
    var postTitles = [String]()
    var senderId:String?
    var finalPrice:String!
    var paymentSucceeded = false
    var path:Int!
    var displayMessages = [Message]()
    var offerTitles = [String]()
    var newMess:Message?
    var totalAmount:Int!
    var success = false
    var customerID:String!
    var request:PKPaymentRequest!
    
    @IBOutlet weak var messageHeight: NSLayoutConstraint!
    @IBOutlet weak var messageBox: UITextView!
    @IBOutlet weak var buttonView: UIView!
    @IBOutlet weak var topOfTableView: NSLayoutConstraint!
    @IBOutlet weak var sendButton: UIButton!
    @IBOutlet weak var inputViewBottom: NSLayoutConstraint!
    
    //@IBOutlet weak var messageBox: UITextView!
    //@IBOutlet weak var messageBox: UITextField!
    @IBOutlet weak var tableView: UITableView!
    let paymentNetworks = [PKPaymentNetwork.visa, PKPaymentNetwork.masterCard]
    let ApplePaySwagMerchantID = "merchant.erichards.uoPond"
    
    enum Constants {
      static let publishableKey = "pk_test_SrWT3POBWZ2LYJDKrAzMG4EY00iQb93sE6"
      static let baseURLString = "http://localhost:4567"
      static let defaultCurrency = "usd"
      static let defaultDescription = "Purchase from Pond Marketplace"
    }
    var db : Firestore!
    var clientToken:String!
    var braintreeClient:BTAPIClient!
    

    
    override func viewDidLoad() {
        super.viewDidLoad()
        db = Firestore.firestore()
        self.tableView.contentInset = UIEdgeInsets(top: 10, left: 0, bottom: 0, right: 0)
        self.tableView.delegate = self
        self.tableView.dataSource = self
        self.senderId = myID
        self.grabMessages()
        print("finalMessages:", self.finalMessages)
        self.tableView.separatorColor = UIColor.clear
        dismissKeyboard()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.tableView.reloadData()
            if self.finalMessages.count > 0 {
                let ind = IndexPath(row: self.finalMessages.count - 1, section: 0)
                self.tableView.scrollToRow(at: ind, at: .bottom, animated: true)
            }
        }
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.reloadData()
        if self.senderId == nil {
            self.senderId = Auth.auth().currentUser?.uid
            self.tableView.reloadData()
        }
        setUpView()
        addApplePayButton()
        self.tableView.allowsSelection = false
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.tableView.allowsSelection = true
        }
        self.grabCustomerID()
    }
    
    func grabCustomerID() {
        // retrive customer id from firestore
        let ref = db.collection("stripe_customers")
        ref.getDocuments { (snap, err) in
            if let docs = snap?.documents {
                for snapshot in docs {
                    if snapshot.documentID == self.myID {
                        self.customerID = snapshot["customer_id"] as? String ?? ""
                        print(self.customerID)
                    }
                }
            }
        }
        
        
    }
    
    
    func addApplePayButton ()
    {
        var btnApplePay = PKPaymentButton()
        btnApplePay = PKPaymentButton(paymentButtonType: .plain, paymentButtonStyle: .black)
        //btnApplePay.imageView?.roundCorners(corners: [.topLeft, .topRight], amount: 15)
        btnApplePay.addTarget(self, action:#selector(getAmount), for: .touchUpInside)
        btnApplePay.frame = CGRect(x: 0, y: 0, width: 40, height: self.buttonView.frame.size.height)
        btnApplePay.layer.cornerRadius = 10
        btnApplePay.layer.masksToBounds = true
        btnApplePay.backgroundColor = UIColor.clear
        self.buttonView.addSubview(btnApplePay)
        btnApplePay.isHidden = !PKPaymentAuthorizationViewController.canMakePayments(usingNetworks: paymentNetworks)
        //btnApplePay.rightAnchor.constraint(equalTo: self.buttonView.rightAnchor).isActive = true
        
    }
    
    
    override func viewDidDisappear(_ animated: Bool) {
        let VC = self.storyboard!.instantiateViewController(withIdentifier: "inboxSB") as! inboxTableViewController
        VC.newMess = false
        
    }
    
    func setUpView(){
        BTUIKAppearance.sharedInstance().colorScheme = .dark
        self.messageBox.layer.borderColor = UIColor.lightGray.cgColor
        //self.messageBox.attributedPlaceholder = NSAttributedString(string: "send message", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        self.navigationController?.navigationBar.backgroundColor = UIColor.black
        self.navigationController?.navigationBar.barTintColor = UIColor.black
        self.messageBox.layer.cornerRadius = 10
        self.buttonView.backgroundColor = UIColor.clear
        self.messageHeight.constant = self.heightForView(text: "F", font: UIFont.systemFont(ofSize: 15), width: UIScreen.main.bounds.width - 50) + 20
        self.messageBox.delegate = self
        self.messageBox.contentInset = UIEdgeInsets(top: 0, left: 8, bottom: 0, right: 8)
    }
    
    
    @objc func getAmount(){
        print("get money")
        displayGetMoney(title: "Negotiated Price:", message: "")
        
    }
   
    

    
    
    func getFee(price:String) -> Float{
        let p = Float(price)!
        let fee = p * 0.05
        return fee
    }
    
    func getTotal(price:String, fee:Float) -> Float {
        let p = Float(price)!
        self.totalAmount = Int(p + fee)
        return p + fee
    }
    
    func paying(){
        print("paying..")
     
        
        let request = PKPaymentRequest()
        request.merchantIdentifier = ApplePaySwagMerchantID
        request.supportedNetworks = paymentNetworks
        request.merchantCapabilities = PKMerchantCapability.capability3DS
        request.countryCode = "US"
        request.currencyCode = "USD"
        request.requiredBillingContactFields = .init(arrayLiteral: .emailAddress)
        let paymentItem = PKPaymentSummaryItem.init(label: self.offerTitle!, amount: NSDecimalNumber(value: Float(self.finalPrice!)!))
        let processFee = PKPaymentSummaryItem.init(label: "fee", amount: NSDecimalNumber(value: self.getFee(price: self.finalPrice!)))
        let total = PKPaymentSummaryItem.init(label: "total", amount: NSDecimalNumber(value: self.getTotal(price: self.finalPrice!, fee: self.getFee(price: self.finalPrice!))), type: .final)
        request.paymentSummaryItems = [paymentItem, processFee, total]
        let applePayController = PKPaymentAuthorizationViewController(paymentRequest: request)
        
    
        self.request = request
        //self.present(applePayController!, animated: true, completion: nil)

        //self.fetchClientToken()
        
        //self.clientToken = "CLIENT_TOKEN_FROM_SERVER"
        self.showDropIn(clientTokenOrTokenizationKey: "CLIENT_TOKEN_FROM_SERVER")
        
        


    }
    
    func displayDefaultAlert(title: String?, message: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "Ok", style: .default, handler: nil)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func displayGetMoney(title: String?, message: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addTextField { (field) in
            field.placeholder = "$"
            field.keyboardType = .numberPad
            field.textAlignment = .center
        }
        alert.addAction(UIAlertAction(title: "Done", style: .default, handler: { [weak alert] (_) in
            let textField = alert?.textFields![0]
            
            guard textField!.text! != "" else {
                print("canceled")
                return
            }
            self.finalPrice = textField!.text!
            
            self.paying()
        }))
        self.present(alert, animated: true, completion: nil)
    }
    
    
    func dismissKeyboard(){
        let tap = UITapGestureRecognizer(target: self, action: #selector(done))
        self.view.addGestureRecognizer(tap)
    }
    
    @objc func done(){
        self.view.endEditing(true)
        self.tableView.reloadData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.isToolbarHidden = true
    }
    
    
    
    @IBAction func sent(_ sender: Any) {

        
        let text = self.messageBox.text
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.messageBox.text = ""
        }
        guard text != "" else {
            return
        }
        guard self.sendButton.tintColor == UIColor.systemBlue else {
            return
        }
            let VC = self.storyboard!.instantiateViewController(withIdentifier: "inboxSB") as! inboxTableViewController
            VC.newMess = false
            let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Messages")
            let currentDate = Date().timeIntervalSinceReferenceDate
            let seconds = Double(currentDate)
            let timestamp = NSDate(timeIntervalSince1970: seconds)
            let dateformat = DateFormatter()
            dateformat.dateFormat = "hh:mm a"
            dateformat.string(from: timestamp as Date)
        let message = ["sender_id":senderId, "sender_name":self.myName!, "text":text!, "time":String(describing:currentDate), "receiverID": self.receiverID, "title" : self.offerTitle, "transaction":"no"]
            ref.child("\(self.offerTitle!)").child("\(self.senderId!)to\(self.receiverID!)").childByAutoId().setValue(message)
            ref.child("\(self.offerTitle!)").child("\(self.senderId!)to\(self.receiverID!)").updateChildValues(["\(self.receiverID!)-read":"no"])
            
        self.finalMessages.append(Message(sender: self.senderId!, receiver: self.receiverID!, timeStamp: String(describing:currentDate), text: text!, transaction: "no"))
            let ind = IndexPath(row: self.finalMessages.count - 1, section: 0)
            self.tableView.beginUpdates()
            self.tableView.insertRows(at: [ind], with: .bottom)
            self.tableView.endUpdates()
            self.tableView.setNeedsLayout()
            self.sendButton.tintColor = UIColor.systemGray
            newMessage.mess = Message(sender: self.senderId!, receiver: self.receiverID!, timeStamp: String(describing:currentDate), text: text!, transaction: "no")
            newMessage.new = true
    }
    
    func sendTransaction(p:Float){
        let price = getFormattedCurrency(number: p)
        var text:String!
        if self.success == false {
            text = "Transaction Failed for \(price)"
        }else {
            text = "Transaction Was Completed for \(price)"
        }
        let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Messages")
            let currentDate = Date().timeIntervalSinceReferenceDate
            let seconds = Double(currentDate)
            let timestamp = NSDate(timeIntervalSince1970: seconds)
            let dateformat = DateFormatter()
            dateformat.dateFormat = "hh:mm a"
            dateformat.string(from: timestamp as Date)
        let message = ["sender_id":senderId, "sender_name":self.myName!, "text": text, "time":String(describing:currentDate), "receiverID": self.receiverID, "title" : self.offerTitle, "transaction":"yes"]
            ref.child("\(self.offerTitle!)").child("\(self.senderId!)to\(self.receiverID!)").childByAutoId().setValue(message)
            ref.child("\(self.offerTitle!)").child("\(self.senderId!)to\(self.receiverID!)").updateChildValues(["\(self.receiverID!)-read":"no"])
            
        self.finalMessages.append(Message(sender: self.senderId!, receiver: self.receiverID!, timeStamp: String(describing:currentDate), text: text, transaction: "yes"))
            let ind = IndexPath(row: self.finalMessages.count - 1, section: 0)
            self.tableView.beginUpdates()
            self.tableView.insertRows(at: [ind], with: .bottom)
            self.tableView.endUpdates()
            self.tableView.setNeedsLayout()
        
    }
    
   
    func textViewDidChange(_ textView: UITextView) {
        if (self.messageBox.text.count > 0) {
            self.sendButton.tintColor = UIColor.systemBlue
             self.messageHeight.constant = self.heightForView(text: self.messageBox.text, font: UIFont.systemFont(ofSize: 15), width: UIScreen.main.bounds.width - 50) + 20
        }else{
            self.sendButton.tintColor = UIColor.systemGray
            self.messageHeight.constant = self.heightForView(text: "F", font: UIFont.systemFont(ofSize: 15), width: UIScreen.main.bounds.width - 50) + 20
        }
    }
  
    func organizeMess(){
            var timeStamps = [String]()
            for mess in 0..<self.firstMess.count{
                timeStamps.append(self.firstMess[mess].timeStamp)
            }
            if timeStamps.count == self.firstMess.count{
                timeStamps.sort {
                    $0 < $1
                }
                
             for i in 0..<timeStamps.count {
                    for m in 0..<self.firstMess.count{
                        if self.firstMess[m].timeStamp == timeStamps[i]{
                         let receiver = self.firstMess[m].receiver
                            let id = self.firstMess[m].sender
                            let text = self.firstMess[m].text
                            let trans = self.firstMess[m].transaction
                            let seconds = Double(self.firstMess[m].timeStamp)
                            let date = NSDate(timeIntervalSince1970: seconds!)
                            let dateformat = DateFormatter()
                            dateformat.dateFormat = "hh:mm a"
                            dateformat.string(from: date as Date)
                            self.finalMessages.append(Message(sender: id, receiver: receiver, timeStamp: dateformat.string(from: date as Date), text: text, transaction: trans))
                        }
                    }
                }
            }
     }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)

        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillAppear(notification:)), name: .UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(keyboardWillDisappear(notification:)), name: .UIKeyboardWillHide, object: nil)
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillShow , object: nil)
        NotificationCenter.default.removeObserver(self, name: .UIKeyboardWillHide , object: nil)
    }

    @objc func keyboardWillAppear(notification: NSNotification?) {

        guard let keyboardFrame = notification?.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue else {
            return
        }

        let keyboardHeight: CGFloat
        if #available(iOS 11.0, *) {
            keyboardHeight = keyboardFrame.cgRectValue.height - self.view.safeAreaInsets.bottom
        } else {
            keyboardHeight = keyboardFrame.cgRectValue.height
        }
       
        inputViewBottom.constant = keyboardHeight + 8
    }

    @objc func keyboardWillDisappear(notification: NSNotification?) {
        inputViewBottom.constant = 8
    }
     
    func getFormattedCurrency(number: Float) -> String {
        let currencyFormatter = NumberFormatter()
         currencyFormatter.usesGroupingSeparator = true
         currencyFormatter.numberStyle = NumberFormatter.Style.currency
        
         currencyFormatter.locale = NSLocale.current
        let priceString = currencyFormatter.string(from: number as NSNumber)
        return priceString!
        
    }
    
  func grabMessages(){
          //querying for the messages
    guard let title = self.offerTitle else {
        print("no offertitle")
        return
    }
    guard let sender = self.senderId else {
        print("no sender id")
        return
    }
    guard let rec = self.receiverID else {
        print("no receiver")
        return
    }
    let query = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Messages").child("\(title)").child("\(sender)to\(rec)")
          query.queryOrdered(byChild: "time").observe(.value) { (snapshot) in
              if let snapshots = snapshot.children.allObjects as? [DataSnapshot]{
                  for child in snapshots{
                      let data = child.value as? NSDictionary
                      let id = data?["sender_id"] as? String ?? ""
                      //let name = data?["sender_name"] as? String ?? ""
                      let text = data?["text"] as? String ?? ""
                      let receiver = data?["receiverID"] as? String ?? ""
                      let time = data?["time"] as? String ?? ""
                        let trans = data?["transaction"] as? String ?? ""
                      if id == self.myID || receiver == self.myID{
                        let message = Message(sender: id, receiver: receiver, timeStamp: time, text: text, transaction: trans)
                          self.firstMess.append(message)
                        
                      }
                  }
              }
          }
          let cast = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Messages").child("\(self.offerTitle!)").child("\(self.receiverID!)to\(self.senderId!)")
          cast.queryOrdered(byChild: "time").observe(.value) { (snapshot) in
              if let snapshots = snapshot.children.allObjects as? [DataSnapshot]{
                  for child in snapshots{
                      let data = child.value as? NSDictionary
                      let id = data?["sender_id"] as? String ?? ""
                      //let name = data?["sender_name"] as? String ?? ""
                      let text = data?["text"] as? String ?? ""
                      let receiver = data?["receiverID"] as? String ?? ""
                      let time = data?["time"] as? String ?? ""
                        let trans = data?["transaction"] as? String ?? ""
                      if id == self.myID || receiver == self.myID{
                          let message = Message(sender: id, receiver: receiver, timeStamp: time, text: text, transaction: trans)
                          self.firstMess.append(message)
                      }
                  }
              }
          }
          DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
              self.organizeMess()
          }
      }
    
    

}
extension chatViewController: UITableViewDelegate, UITableViewDataSource{
    
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        print("rows",self.finalMessages.count)
        return self.finalMessages.count
    }
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
           let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
           label.numberOfLines = 0
           label.lineBreakMode = NSLineBreakMode.byWordWrapping
           label.font = font
           label.text = text

           label.sizeToFit()
           return label.frame.height
       }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {

        if self.finalMessages[indexPath.row].transaction == "yes"{
            return heightForView(text: self.finalMessages[indexPath.row].text, font: UIFont.systemFont(ofSize: 12), width: 250) + 25
        }
        return heightForView(text: self.finalMessages[indexPath.row].text, font: UIFont.systemFont(ofSize: 17), width: 250) + 30
    }
    
 
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "chatCell", for: indexPath) as! messageTableViewCell
        
        let message = self.finalMessages[indexPath.row]
        cell.textMess = message.text
        
        if message.sender == self.myID {
            cell.mine = true
        }else{
            cell.mine = false
        }
        if message.transaction == "yes" {
            cell.transaction = true
        } else {
            cell.transaction = false
        }
  
        return cell
    }
    
    
}
extension UIView {
    enum Corner:Int {
        case bottomRight = 0,
        topRight,
        bottomLeft,
        topLeft
    }

    private func parseCorner(corner: Corner) -> CACornerMask.Element {
        let corners: [CACornerMask.Element] = [.layerMaxXMaxYCorner, .layerMaxXMinYCorner, .layerMinXMaxYCorner, .layerMinXMinYCorner]
        return corners[corner.rawValue]
    }

    private func createMask(corners: [Corner]) -> UInt {
        return corners.reduce(0, { (a, b) -> UInt in
            return a + parseCorner(corner: b).rawValue
        })
    }

    func roundCorners(corners: [Corner], amount: CGFloat = 5) {
        layer.cornerRadius = amount
        let maskedCorners: CACornerMask = CACornerMask(rawValue: createMask(corners: corners))
        layer.maskedCorners = maskedCorners
    }
}
extension chatViewController {
    
   
    
    func fetchClientToken() {
        // TODO: Switch this URL to your own authenticated API
        let clientTokenURL = NSURL(string: "https://braintree-sample-merchant.herokuapp.com/client_token")!
        let clientTokenRequest = NSMutableURLRequest(url: clientTokenURL as URL)
        clientTokenRequest.setValue("text/plain", forHTTPHeaderField: "Accept")

        URLSession.shared.dataTask(with: clientTokenRequest as URLRequest) { (data, response, error) -> Void in
            // TODO: Handle errors
            let clientToken = String(data: data!, encoding: String.Encoding.utf8)
            

            // As an example, you may wish to present Drop-in at this point.
            // Continue to the next section to learn more...
            }.resume()
    }
    
    func showDropIn(clientTokenOrTokenizationKey: String) {
        let request =  BTDropInRequest()
        if let drop = BTDropInController(authorization: clientTokenOrTokenizationKey, request: request, handler: { (controller, result, error) in
            if (error != nil) {
                print("ERROR")
            } else if (result?.isCancelled == true) {
                print("CANCELLED")
            } else if let result = result {
                // Use the BTDropInResult properties to update your UI
                //result.paymentOptionType = .payPal
                let selectedPaymentOptionType = result.paymentOptionType
                let selectedPaymentMethod = result.paymentMethod
                let selectedPaymentMethodIcon = result.paymentIcon
                let selectedPaymentMethodDescription = result.paymentDescription
                request.paypalDisabled = false
                request.venmoDisabled = false
                request.vaultManager = true

            }
            controller.dismiss(animated: true, completion: nil)
        }){
            self.present(drop, animated: true, completion: nil)
        }
//        let dropIn = BTDropInController(authorization: clientTokenOrTokenizationKey, request: request){ (controller, result, error) in
//            if (error != nil) {
//                print("ERROR")
//            } else if (result?.isCancelled == true) {
//                print("CANCELLED")
//            } else if let result = result {
//                // Use the BTDropInResult properties to update your UI
//                //result.paymentOptionType = .payPal
//                let selectedPaymentOptionType = result.paymentOptionType
//                let selectedPaymentMethod = result.paymentMethod
//                let selectedPaymentMethodIcon = result.paymentIcon
//                let selectedPaymentMethodDescription = result.paymentDescription
//                request.paypalDisabled = false
//                request.venmoDisabled = false
//                request.vaultManager = true
//
//            }
//            controller.dismiss(animated: true, completion: nil)
//        }
//        if dropIn != nil {
//            self.present(dropIn!, animated: true, completion: nil)
//        }else{
//            print("returning nil drop In")
//        }
    }
  
    
    
}
class messageBox: UITextField {
   
    var lastTextBeforeEditing: String?

    override init(frame: CGRect) {
        super.init(frame: frame)
        setupTextChangeNotification()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupTextChangeNotification()
    }

    func setupTextChangeNotification() {
        NotificationCenter.default.addObserver(
            forName: Notification.Name.UITextFieldTextDidChange,
            object: self,
            queue: OperationQueue.main) { (notification) in
                self.invalidateIntrinsicContentSize()
        }
        NotificationCenter.default.addObserver(
            forName: Notification.Name.UITextFieldTextDidBeginEditing,
            object: self,
            queue: OperationQueue.main) { (notification) in
                self.lastTextBeforeEditing = self.text
        }
    }

    override var intrinsicContentSize: CGSize {
        var size = super.intrinsicContentSize

        if isEditing, let text = text, let lastTextBeforeEditing = lastTextBeforeEditing {
            let string = text as NSString
            let stringSize = string.size(withAttributes: [.font: UIFont.systemFont(ofSize: 15)])
            let origSize = (lastTextBeforeEditing as NSString).size(withAttributes: [.font: UIFont.systemFont(ofSize: 15)])
            size.width = size.width + (stringSize.width - origSize.width)
        }

        return size
    }

    deinit {
        NotificationCenter.default.removeObserver(self)
    }
}
extension String {

    static func random(length: Int = 20) -> String {
        let base = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789"
        var randomString: String = ""

        for _ in 0..<length {
            let randomValue = arc4random_uniform(UInt32(base.count))
            randomString += "\(base[base.index(base.startIndex, offsetBy: Int(randomValue))])"
        }
        return randomString
    }
}
