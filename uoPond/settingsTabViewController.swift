//
//  settingsTabViewController.swift
//  uoPond
//
//  Created by Ethan Richards on 2/18/20.
//  Copyright © 2020 Ethan Richards. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseStorage
import FirebaseAuth
import FirebaseFunctions


class settingsTabViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {

    @IBOutlet weak var logoutButton: UIButton!
    @IBOutlet weak var changePWord: UIButton!
    @IBOutlet weak var changeVenmo: UIButton!
    lazy var functions = Functions.functions()
    @IBOutlet weak var stripeButtion: UIButton!
    @IBOutlet weak var profilePic: UIImageView!
    @IBOutlet weak var venmoHeight: NSLayoutConstraint!
    @IBOutlet weak var saveButton: UIButton!
    @IBOutlet weak var venmoField: UITextField!
    var userUid:String?
    var email = [String]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpView()
        self.view.backgroundColor = UIColor.black
        addDoneButton()
        getEmail()
        self.gatherData(completion: { () -> Void? in
            view.reloadInputViews()
        })
    }
 
    @IBAction func sendToStripe(_ sender: Any) {
        functions.httpsCallable("createStripeCustomer").call { (result, err) in
            if err != nil {
                print(err!)
            }else{
                print(result!)
            }
        }
    }
    
    
    
    func setUpView(){
        self.profilePic.layer.cornerRadius = self.profilePic.frame.size.width / 2
        self.profilePic.clipsToBounds = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapped(sender:)))
        self.profilePic.addGestureRecognizer(tap)
        self.profilePic.isUserInteractionEnabled = true
        logoutButton.layer.cornerRadius = 10
        logoutButton.layer.borderWidth = 1
        logoutButton.layer.borderColor = UIColor.systemBlue.cgColor
       //  saveButton.layer.cornerRadius = 10
        changeVenmo.layer.cornerRadius = 10
        changePWord.layer.cornerRadius = 10
       // saveButton.layer.borderColor = UIColor.systemBlue.cgColor
       // saveButton.layer.borderWidth = 1
        changeVenmo.layer.borderColor = UIColor.systemBlue.cgColor
        changeVenmo.layer.borderWidth = 1
        changePWord.layer.borderColor = UIColor.systemBlue.cgColor
        changePWord.layer.borderWidth = 1
        // saveButton.isHidden = true
        self.venmoField.attributedPlaceholder = NSAttributedString(string: "@venmo..", attributes: [NSAttributedString.Key.foregroundColor: UIColor.darkGray])
        self.saveButton.isHidden = true
        self.venmoHeight.constant = 0
        self.saveButton.layer.cornerRadius = 10
    }
    
    
       @objc func tapped(sender: UIGestureRecognizer){
           let picker = UIImagePickerController()
           picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
           picker.allowsEditing = true
           picker.delegate = self
           present(picker, animated: true, completion: nil)
       }

    func addDoneButton(){
         let keyboardToolBar = UIToolbar()
         keyboardToolBar.sizeToFit()
          
         let flexibleSpace = UIBarButtonItem(barButtonSystemItem:
             UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
         let doneButton = UIBarButtonItem(barButtonSystemItem:
             UIBarButtonSystemItem.done, target: self, action: #selector(self.doneClicked) )
          
         keyboardToolBar.setItems([flexibleSpace, doneButton], animated: true)
          
         
     }
    
    
    @objc func doneClicked(){
           view.endEditing(true)
       }
    
    @IBAction func changingVenmo(_ sender: Any) {
        self.venmoHeight.constant = 40
        self.updateFrame()
        self.saveButton.isHidden = false
    }
    
    func updateFrame(){
        self.changeVenmo.layer.cornerRadius = 0
        self.changeVenmo.layer.borderWidth = 0
        self.changeVenmo.add(border: .top, color: UIColor.systemBlue, width: 1)
        self.changeVenmo.add(border: .left, color: UIColor.systemBlue, width: 1)
        self.changeVenmo.add(border: .right, color: UIColor.systemBlue, width: 1)
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.venmoField.addBottomBorder()
            self.venmoField.addRightBorder()
            self.venmoField.addLeftBorder()
        }
    }
   
    func roundCorners(view :UIView, corners: UIRectCorner, radius: CGFloat){
               let path = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
               let mask = CAShapeLayer()
               mask.path = path.cgPath
               view.layer.mask = mask
       }
    
    @IBAction func saveVenmo(_ sender: Any) {
        
        if (self.venmoField.isHidden == false) && (self.venmoField.text != "") {
            let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Users")
            let userReference = ref.child(self.userUid!)
            userReference.updateChildValues(["venmo":self.venmoField.text!])
        }
        
        saveButton.isHidden = true
        self.venmoHeight.constant = 0
        self.changeVenmo.layer.borderWidth = 1
        self.changeVenmo.remove(border: .left)
        self.changeVenmo.remove(border: .right)
        self.changeVenmo.remove(border: .top)
        self.changeVenmo.layer.cornerRadius = 10
        self.venmoField.removeBorders()
        self.venmoField.removeBorders()
        self.venmoField.removeBorders()
    }
    
    
    @IBAction func changePWord(_ sender: Any) {
        Auth.auth().sendPasswordReset(withEmail: self.email[0]) { (err) in
            if err != nil {
                print(err!)
                return
            }else{
                let alertController = UIAlertController(title: "Sent!", message: "Please check your email to reset your password.", preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alertController.addAction(defaultAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
           picker.dismiss(animated: true, completion: nil)
       }
    
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
            
            var selectedImage: UIImage?
            if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage{
                selectedImage = editedImage
            }else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
                selectedImage = originalImage
            }
            self.profilePic.image = selectedImage
            
             guard let image = selectedImage else{
                 print("error")
                 return
             }
             let imageName = UUID().uuidString
             let data = UIImageJPEGRepresentation(image, 1.0)
             let imageRef = Storage.storage().reference().child("users").child("\(self.userUid!)").child("profPicture.jpg")
             imageRef.putData(data!, metadata: nil) { (metadata, error) in
                 if error != nil {
                     print(error!)
                     return
                 }else{
                     imageRef.downloadURL(completion: { (url, err) in
                         if err != nil{
                             print(err!)
                             return
                         }else{
                             guard let url = url else{
                                 print("unsuccessful")
                                 return
                             }
                             let urlString = url.absoluteString
                             let uid = self.userUid!
                             let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Users")
                             let userReference = ref.child(uid)
                             let values = ["profilePicture" : urlString]
                             userReference.updateChildValues(values, withCompletionBlock: { (err, ref) in
                                 if err != nil{
                                     print(err!)
                                     return
                                 }else{
                                     print("success")
                                 }
                             })
                         }
                     })
                 }
                 
             }
         
            picker.dismiss(animated: true, completion: nil)
        }
    
    
     func getEmail(){
         let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Users")
         let userReference = ref.child(self.userUid!)
         userReference.observe( .value) { (snapshot) in
             
             let value = snapshot.value as? NSDictionary
             let userEmail = value?["email"] as? String ?? ""
             self.email = [userEmail]
         }
     }
    
    @IBAction func logoutPressed(_ sender: Any) {
        self.showSpinner(onView: self.view)
        self.performSegue(withIdentifier: "signedOut", sender: self)
        do{
            try Auth.auth().signOut()
        } catch let error{
            print("error", error)
        }
    }
    
    
    
    func gatherData(completion: () -> Void?){
         let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Users")
         ref.child("\(self.userUid!)/").observe( .value) { (snapshot) in
                 if let value = snapshot.value as? [String:Any] {
             
             //guard let value = snapshot.children.allObjects as! NSDictionary else { print("ERROR"); return }
                     let downloadURL = value["profilePicture"] as? String ?? ""
                     let storageRef = Storage.storage().reference(forURL: downloadURL)
                     storageRef.getData(maxSize: 20000000, completion: { (data, error) in
                         if error != nil{
                             print("error gettin image in gatherdata")
                         }else{
                             let postImage = UIImage(data: data!)
                             self.profilePic.startAnimating()
                             self.profilePic.image = postImage
                             self.profilePic.stopAnimating()
                         }
                     })
                 }else{
                     print("Not creating dictionary")
                 }
         }
         completion()
     }
    
}
enum ViewBorder: String {
    case left, right, top, bottom
}
extension UIView {
    func add(border: ViewBorder, color: UIColor, width: CGFloat) {
        let borderLayer = CALayer()
        borderLayer.backgroundColor = color.cgColor
        borderLayer.name = border.rawValue
        switch border {
        case .left:
            borderLayer.frame = CGRect(x: 0, y: 0, width: width, height: self.frame.size.height)
        case .right:
            borderLayer.frame = CGRect(x: self.frame.size.width - width, y: 0, width: width, height: self.frame.size.height)
        case .top:
            borderLayer.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: width)
        case .bottom:
            borderLayer.frame = CGRect(x: 0, y: self.frame.size.height - width, width: self.frame.size.width, height: width)
        }
        self.layer.addSublayer(borderLayer)
    }

    func remove(border: ViewBorder) {
        guard let sublayers = self.layer.sublayers else { return }
        var layerForRemove: CALayer?
        for layer in sublayers {
            if layer.name == border.rawValue {
                layerForRemove = layer
            }
        }
        if let layer = layerForRemove {
            layer.removeFromSuperlayer()
        }
    }
}
extension UITextField {
    func addBottomBorder(){
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0, y: self.frame.size.height - 1, width: self.frame.size.width, height: 1)
        bottomLine.backgroundColor = UIColor.systemBlue.cgColor
        borderStyle = .none
        layer.addSublayer(bottomLine)
    }
    func addRightBorder(){
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: self.frame.size.width - 1, y: 0, width: 1, height: self.frame.height)
        bottomLine.backgroundColor = UIColor.systemBlue.cgColor
        borderStyle = .none
        layer.addSublayer(bottomLine)
    }
    func addLeftBorder(){
        let bottomLine = CALayer()
        bottomLine.frame = CGRect(x: 0, y: 0, width: 1, height: self.frame.height)
        bottomLine.backgroundColor = UIColor.systemBlue.cgColor
        borderStyle = .none
        layer.addSublayer(bottomLine)
    }
    func removeBorders(){
        guard let sublayers = self.layer.sublayers else { return }
        var layerForRemove: CALayer?
        for layer in sublayers {
            layerForRemove = layer
        }
        if let layer = layerForRemove {
            layer.removeFromSuperlayer()
        }
    }
}
