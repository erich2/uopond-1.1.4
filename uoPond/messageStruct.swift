//
//  messageStruct.swift
//  uoPond
//
//  Created by Ethan Richards on 7/12/19.
//  Copyright © 2019 Ethan Richards. All rights reserved.
//

import Foundation
import UIKit

struct Message {
    var sender: String
    var receiver: String
    var timeStamp: String
    var text: String
    var transaction: String
}
