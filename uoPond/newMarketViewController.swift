//
//  newMarketViewController.swift
//  uoPond
//
//  Created by Ethan Richards on 2/19/20.
//  Copyright © 2020 Ethan Richards. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseStorage
import FirebaseAuth

class newMarketViewController: UIViewController,  UICollectionViewDelegate, UICollectionViewDataSource {
   
    
    
    var marketPosts = [Post]()
    var marketImage = [UIImage]()
    //typealias functionFinished = () -> ()
    var refresh: UIRefreshControl!
    var isSearching = false
    var searchPost = [Post]()
    var userTitle = [String]()
    var uid = [String]()
    var myID:String!
    var myName:String?
    var profImage:UIImage?
    var clickedPath: IndexPath?
    var descripHeights = [CGFloat]()
    var blacklist = [String]()
    var color:UIColor!
    var emote:UIImage!
    var myUserName:String?
    var postID = [String]()


    @IBOutlet weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpNaviBar()
        self.collectionView?.isPagingEnabled = true
        self.collectionView?.backgroundColor = UIColor.black
        //searchBAR.delegate = self
        let refresh = UIRefreshControl()
        refresh.tintColor = UIColor.black
        collectionView?.addSubview(refresh)
        collectionView?.delegate = self
        DispatchQueue.global(qos: .background).async {
            self.gatherPosts(completion: { () -> Void in
                for u in 0..<self.marketPosts.count{
                    let user = self.marketPosts[u].user
                    //self.addVenmos(user: user)
                }
                self.collectionView?.reloadData()
            })
        }
        addDoneButton()
        print("Did load:",self.marketPosts.count)
        //self.searchBAR.frame.size.height = 0
        //self.searchBAR.searchTextField.textColor = UIColor.white
        getToolbar()
        //self.collectionView!.register(UICollectionViewCell.self, forCellWithReuseIdentifier: reuseIdentifier)
        let flowLayout = UICollectionViewFlowLayout()
        flowLayout.itemSize = CGSize(width: UIScreen.main.bounds.width, height: self.collectionView.bounds.height + 40)
        flowLayout.scrollDirection = .horizontal
        flowLayout.minimumLineSpacing = 10
        flowLayout.minimumInteritemSpacing = 0
        flowLayout.sectionInset = .init(top: 0, left: 0, bottom: 0, right: 0)
        self.collectionView.collectionViewLayout = flowLayout
        print(self.marketPosts)
        collectionView!.delegate = self
        collectionView!.dataSource = self
        collectionView.reloadData()

    }
    

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.marketPosts.count
    }
       
    

    func collectionView(collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, referenceSizeForHeaderInSection section: Int) -> CGSize {
        return CGSize.zero
    }
    
    
       
   func setUpNaviBar(){
       //self.navigationController?.navigationBar.backgroundColor = self.color
       //self.navigationController?.navigationBar.barTintColor = self.color
       self.navigationController?.navigationBar.isTranslucent = true
       self.navigationController?.isToolbarHidden = false
       //self.navigationController?.navigationBar.tintColor = UIColor.white
       self.collectionView?.backgroundColor = UIColor.black
       //self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font:UIColor.white]
    self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "AmericanTypewriter", size: 25)!, NSAttributedString.Key.foregroundColor : self.color]
       self.navigationItem.hidesBackButton = true
       let emote = UIBarButtonItem(image: self.emote, style: .done, target: self, action: nil)
        emote.tintColor = self.color
       self.navigationItem.rightBarButtonItem = emote
       //self.searchBAR.barTintColor = UIColor.black
   }

    
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        targetContentOffset.pointee = scrollView.contentOffset
        var factor: CGFloat = 0.5
        if velocity.x < 0 {
            factor = -factor
        }
        let indexPath = IndexPath(row: Int(scrollView.contentOffset.x/400 + factor), section: 0)
        collectionView?.scrollToItem(at: indexPath, at: .left, animated: true)
    }

    
    func getToolbar(){
            if #available(iOS 13.0, *) {
                //let searchBarButton = UIBarButtonItem(image: UIImage(systemName: "magnifyingglass"), style: .plain, target: self, action: #selector(searchTapped(_:)))
    //            let profile = UIBarButtonItem(image: UIImage(systemName: "person.circle.fill"), style: .plain, target: self, action: #selector(profButtonPressed(_:)))
                let newBackButton = UIBarButtonItem(image: UIImage(systemName: "return"), style: .plain, target: self, action: #selector(backPressed(sender:)))
                let rightSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
    //            let leftSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
                self.toolbarItems = [newBackButton,  rightSpace ]
                self.navigationController?.toolbar.barTintColor = UIColor.black
            } else {
                // Fallback on earlier versions
            }
        }
    
    override func viewWillAppear(_ animated: Bool) {
              self.navigationController?.isToolbarHidden = false
          }
       
       override func viewDidAppear(_ animated: Bool) {
              self.navigationController?.isToolbarHidden = false
          }
          
       
       @objc func backPressed(sender: UIBarButtonItem){
//              let VC = self.storyboard!.instantiateViewController(withIdentifier: "collectionSB") as! marketBiggerTableViewController
//                VC.navigationController?.navigationBar.tintColor = UIColor.black
//              VC.data = []
//              DispatchQueue.main.asyncAfter(deadline: .now()+0.2) {
//                  VC.data = ["furniture", "notes", "tech", "clothing", "books"]
//                  VC.tableView.reloadData()
//              }
//
//
//              self.navigationController?.pushViewController(VC, animated: false)
            self.navigationController?.popViewController(animated: true)
          }
       
       func addDoneButton(){
              let keyboardToolBar = UIToolbar()
              keyboardToolBar.sizeToFit()
               
              let flexibleSpace = UIBarButtonItem(barButtonSystemItem:
                  UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
              let doneButton = UIBarButtonItem(barButtonSystemItem:
                  UIBarButtonSystemItem.done, target: self, action: #selector(self.doneClicked) )
               
              keyboardToolBar.setItems([flexibleSpace, doneButton], animated: true)
               
              //searchBAR.inputAccessoryView = keyboardToolBar
          }
       
       @objc func doneClicked(){
           view.endEditing(true)
       }

       
       func gatherPosts(completion: @escaping () -> Void?){
             let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Posts")
             ref.observe(.value) { (snapshot) in
                 if let snapshots = snapshot.children.allObjects as? [DataSnapshot]{
                     for child in snapshots{
                         let value = child.value as? NSDictionary
                         
                         let section = value?["section"] as? String ?? ""
                         if section != "" && section == self.navigationItem.title {
                                var urls = [String]()
                             let user = value?["user"] as? String ?? ""
                             let title = value?["title"] as? String ?? ""
                             let descrip = value?["description"] as? String ?? ""
                             let price = value?["price"] as? String ?? ""
                            let imgNumber = value?["imgNumber"] as? String ?? ""
                             for img in 0..<Int(imgNumber)! {
                                 let image = value?["image\(img)"] as? String ?? ""
                                 urls.append(image)
                             }
                             let userName = value?["userName"] as? String ?? ""
                             let postID = value?["postID"] as? String ?? ""
                             let flagged = value?["flagged"] as? String ?? ""
                            let marketPost = Post(descrip: descrip, title: title, price: price, imageURLs: urls, user: user, userName: userName, postID: postID)
                             if (self.blacklist.contains(user)) || (flagged == "yes") {
                                 print("blocked user")
                             }else{
                                 self.marketPosts += [marketPost]
                                self.postID.append(postID)
                             }
                             
                         }

                     }
                     completion()
                     }
                 else{
                     print("ERROR")
                 }
                 }
             
             print("after function:",self.marketPosts.count)
         }
       
       func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
              if searchBar.text == nil || searchBar.text == ""{
                  isSearching = false
               collectionView!.endEditing(true)
               collectionView!.reloadData()
              }else{
                  isSearching = true
                  print("searching......")
                  searchPost = marketPosts.filter({$0.title.uppercased().contains(searchText.uppercased())} )
               collectionView!.reloadData()
              }
          }
       
       
       @objc func message(sender:Any?){
        
        
             let button = sender as! UIButton
             let path = button.tag
             if MyVariables.userID != nil{
                 if self.marketPosts[path].user == MyVariables.userID{
                        
                         print("can't message myself")
                 
                     }else{
                        
                         let VC = self.storyboard!.instantiateViewController(withIdentifier: "contactSB") as! chatViewController
                         print("THE PATH:",path)
                         VC.myName = self.myName
                         VC.myID = self.myID
                         VC.receiverName = self.marketPosts[(path)].userName
                         print("USERNAME:",self.marketPosts[path].userName)
                         VC.offerTitle = self.marketPosts[(path)].title
                         VC.receiverID = self.marketPosts[(path)].user
                         VC.senderId = MyVariables.userID
                         VC.navigationItem.title = self.marketPosts[(path)].userName
//                        let downloadURL = self.marketPosts[(path)].imageURL
//                        let storageRef = Storage.storage().reference(forURL: downloadURL)
//                        storageRef.getData(maxSize: 20000000, completion: { (data, error) in
//                            if error != nil{
//                                print("FIND ME RIGHT HERE:", error!)
//                               self.postImages.append(UIImage(named: "Default-welcomer")!)
//                                return
//                            }else{
//                               if let postImage = UIImage(data: data!) {
//                                VC.postImage.image = postImage
//                               }
//                            }
//                        })
//                        let user = self.marketPosts[path].user
//                        let profPicRef = Storage.storage().reference(withPath: "/users/\(user)/profPicture")
//                        profPicRef.getData(maxSize: 20000000) { (data, err) in
//                            if err != nil {
//                                print("uh oh error occured")
//                                VC.profImage.image = UIImage(named: "Default-welcomer")
//                               print("should be done adding image ")
//                            }else{
//                               if let userImage = UIImage(data:data!) {
//                                VC.profImage.image = userImage
//                               }
//                                print("should be done adding image ")
//                            }
//                        }
                   
                         
                        guard VC.senderId == MyVariables.userID as? String else {
                             
                             print("no sender id")
                             return
                         }
                         
                    VC.modalPresentationStyle = .pageSheet
                    
                    self.present(VC, animated: true, completion: nil)
                     }
             }else {
                 print("err in id")
                 return
             }
             
         }
       
    @objc func dismissVC(){
         let VC = self.storyboard!.instantiateViewController(withIdentifier: "contactSB") as! chatViewController
        VC.dismiss(animated: true, completion: nil)
    }
    
      
       
       
       
       // MARK: UICollectionViewDataSource

      func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "marketCollectionCell", for: indexPath) as! marketCollectionViewCell
             let device = self.marketPosts[indexPath.row]
             // Configure the cell
             
            for url in 0..<device.imageURLs.count {
                let downloadURL = device.imageURLs[url]
                let storageRef = Storage.storage().reference(forURL: downloadURL)
                storageRef.getData(maxSize: 20000000, completion: { (data, error) in
                    if error != nil{
                        print("FIND ME RIGHT HERE:", error!)
                        return
                    }else{
                       if let postImage = UIImage(data: data!) {
                        cell.images.append(postImage)
                        //print("from collectio view ",cell.images.count)
                        cell.totalImages.text = "\(device.imageURLs.count)"
                        if url == 0{
                            cell.postImage.image = postImage
                        }
                       }
                    }
                })
            }
            
             
             let user = device.user
             let profPicRef = Storage.storage().reference(withPath: "/users/\(user)/profPicture.jpg")
             profPicRef.getData(maxSize: 20000000) { (data, err) in
                 if err != nil {
                     print("uh oh error occured")
                     cell.actualUserImage.image = UIImage(named: "Default-welcomer")
                    
                    print("should be done adding image ")
                 }else{
                    if let userImage = UIImage(data:data!) {
                         cell.actualUserImage.image = userImage
                        
                    }
                     print("should be done adding image ")
                 }
             }
             cell.descripBox.text = device.descrip
            cell.layer.cornerRadius = 20
             cell.titleLabel.text = device.title
             cell.priceLabel.text = device.price
        cell.postID = self.postID[indexPath.row]
             self.userTitle = [device.user]
             cell.messageButton.tag = indexPath.row
             cell.messageButton.addTarget(self, action: #selector(message), for: .touchUpInside)

             cell.userImage.tag = indexPath.row
             cell.userImage.addTarget(self, action: #selector(userOptions), for: .touchUpInside)


             return cell
         }
    

          @objc func userOptions(sender:Any?){
              let button = sender as! UIButton
              let path = button.tag
              
              let optionMenu = UIAlertController(title: nil, message: "Questionable Content?", preferredStyle: UIAlertControllerStyle.actionSheet)
                  let blockUser = UIAlertAction(title: "Block User", style: UIAlertActionStyle.default, handler: { (alert: UIAlertAction!) -> Void in
                      print("blocking..")
                      let key = "no\(UUID().uuidString)"
                      let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Users").child("\(String(describing: self.myID!))")
                      ref.child("blackList").setValue([key:"\(self.marketPosts[path].user)"]) { (err, ref) in
                          if err != nil {
                              print("err")
                          }else{
                              print("blocked")
                              self.marketPosts.remove(at: path)
                           self.collectionView?.deleteItems(at: [IndexPath(row: path, section: 0)])
                               //self.tableView.deleteRows(at: [IndexPath(row: path, section: 0)], with: .automatic)
                          }
                      }
                      
                  })
                  let flagPost = UIAlertAction(title: "Flag Post", style: UIAlertActionStyle.default, handler: {(alert: UIAlertAction!) -> Void in
                      print("flagging..")
                      let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Posts").child("Post\(self.marketPosts[path].postID)")
                      ref.updateChildValues(["flagged":"yes"], withCompletionBlock: { (err, ref) in
                          if err != nil {
                               print("error in flag")
                          }else {
                              print("flagged")
                              self.marketPosts.remove(at: path)
                           self.collectionView?.deleteItems(at: [IndexPath(row: path, section: 0)])
                              //self.tableView.deleteRows(at: [IndexPath(row: path, section: 0)], with: .automatic)
                              //self.tableView.endUpdates()
                          }
                      })
                  })
                  let cancelOption = UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.cancel, handler: {(alert: UIAlertAction!) -> Void in
                      print("Cancel")
                      optionMenu.dismiss(animated: true, completion: nil)
                  })
                  optionMenu.addAction(blockUser)
                  optionMenu.addAction(flagPost)
                  optionMenu.addAction(cancelOption)
                  
                  present(optionMenu, animated: true, completion: nil)
              }
    
}
extension newMarketViewController : UIViewControllerTransitioningDelegate {
    func presentationController(forPresented presented: UIViewController, presenting: UIViewController?, source: UIViewController) -> UIPresentationController? {
        return HalfSizePresentationController(presentedViewController: presented, presenting: presenting)
    }
}
class HalfSizePresentationController : UIPresentationController {
    override var frameOfPresentedViewInContainerView: CGRect {
        get {
            guard let theView = containerView else {
                return CGRect.zero
            }

            return CGRect(x: 0, y: theView.bounds.height/2, width: theView.bounds.width, height: 600)
        }
    }
}
