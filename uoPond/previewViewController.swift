//
//  previewViewController.swift
//  uoPond
//
//  Created by Ethan Richards on 1/28/20.
//  Copyright © 2020 Ethan Richards. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage

class previewViewController: UIViewController, UIPickerViewDelegate {

    

    @IBOutlet weak var viewTag: UIView!
    
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var descripBox: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var descripToggle: UIButton!
    var post:tempPost!
    var uid:String!
    var userName:String?
    var images = [UIImage]()
    var imageIndex = 0
    var maxImages:Int?
    var postNumber = UUID().uuidString
    
    var section:String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.uid = Auth.auth().currentUser?.uid
        self.setUpImage()
        self.setUpDescrip()
        self.setUpPriceandViewTag()
        self.setUpToolbar()
        self.setUpView()
    }
    

    func setUpView(){
        self.maxImages = self.images.count
        self.priceLabel.text = self.post.price
        self.descripBox.text = "  \(self.post.descrip)"
        self.titleLabel.text = self.post.title
        self.postImage.image = self.images[imageIndex]
        self.userName = self.post.userName
        self.navigationItem.title = "Preview"
        self.descripBox.layer.cornerRadius = 10
    }
    
    @objc
    func rotateImage(gesture: UIGestureRecognizer){
        print("swipe")
        if let swipeGesture = gesture as? UISwipeGestureRecognizer{
        switch swipeGesture.direction{
        case UISwipeGestureRecognizerDirection.up:
            print("User swiped up")

            if imageIndex < self.images.count - 1 {
                imageIndex += 1
            }

            if imageIndex < self.images.count  {
                self.postImage.image = self.images[self.imageIndex]
    
            }
        case UISwipeGestureRecognizerDirection.down:
            print("User swiped down")

            if imageIndex > 0{
                imageIndex -= 1
            }
            if imageIndex >= 0{
                postImage.image = self.images[imageIndex]
                self.postImage.setNeedsDisplay()
            }
           // if curretnImageIndex

            default:
                break
            }
        }
    }

    
    func setUpDescrip(){
        self.descripBox.numberOfLines = 0
        self.descripBox.backgroundColor = UIColor.black.withAlphaComponent(0.40)
        self.descripBox.lineBreakMode = NSLineBreakMode.byTruncatingTail
        self.descripBox.topAnchor.constraint(equalTo: self.postImage.bottomAnchor, constant: 10).isActive = true
    }
 
    
    func setUpToolbar(){
        let post = UIBarButtonItem(title: "Publish", style: .plain, target: self, action: #selector(publish))
        let rS = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        let lS = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        post.tintColor = UIColor.black
        self.toolbarItems = [lS,post,rS]
        self.navigationController?.toolbar.barTintColor = UIColor.systemBlue
        self.navigationController?.toolbar.isUserInteractionEnabled = true

    }
    
    @objc func publish(){
        self.navigationController?.toolbar.isUserInteractionEnabled = false

        self.showSpinner(onView: self.view)
        let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Posts")
        let userReference = ref.child("Post\(postNumber)")
        let values = ["title" : self.titleLabel.text!,
                      "description" : self.descripBox.text!,
                      "price" : self.priceLabel.text!,
                      "user" : self.uid,
                      "userName" : self.userName!,
                      "postID" : postNumber,
                      "section": self.post.category,
                      "time" : String(describing: Date().timeIntervalSinceReferenceDate),
                      "imgNumber" : "\(self.images.count)"]
        userReference.setValue(values, withCompletionBlock: { (err, ref) in
            if err != nil{
                print(err!)
                print("ERROR")
                self.removeSpinner()
                return
            }else{
                var count = 0
                for img in 0..<self.images.count {
                    let imageName = UUID().uuidString
                    print(imageName)
                    let data = UIImageJPEGRepresentation(self.images[img], 1.0)
                    let imageRef = Storage.storage().reference().child("Posts").child("\(self.postNumber)").child("\(imageName).jpg")
                    imageRef.putData(data!, metadata: nil) { (metadata, error) in
                        if error != nil {
                        print(error!)
                        print("error in put data")
                            return
                        }else{
                            imageRef.downloadURL(completion: { (url, err) in
                            if err != nil{
                                print(err!)
                                return
                            }else{
                                guard let url = url else{
                                    print("error getting url guard")
                                    return
                                }
                                print("in the download success")
                                let urlString = url.absoluteString
                                let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Posts")
                                let userReference = ref.child("Post\(self.postNumber)")
                                let value = ["image\(img)" : urlString]
                                userReference.updateChildValues(value) { (err, ref) in
                                    if err != nil {
                                        print("error updating child values")
                                        self.removeSpinner()
                                    }else{
                                        print("success")
                                        count += 1
                                        print(count)
                                        print(self.images.count)
                                        if count == self.images.count {
                                            self.performSegue(withIdentifier: "publishComplete", sender: self)
                                        }
                                    }
                                }
                            }
                        })
                                       
                        }
                    }
                            
                }
            }
        })
        //self.performSegue(withIdentifier: "publishComplete", sender: self)

    }
   
    
    func setUpPriceandViewTag(){
        viewTag.backgroundColor = UIColor.black.withAlphaComponent(0.40)
        self.priceLabel.adjustsFontSizeToFitWidth = true
        
    }
    
    func setUpImage(){
        postImage.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped(sender:)))
        postImage.addGestureRecognizer(tap)
        postImage.isMultipleTouchEnabled = true
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(rotateImage(gesture:)))
        swipeUp.direction = .up
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(rotateImage(gesture:)))
        swipeDown.direction = .down
        self.postImage.addGestureRecognizer(swipeUp)
        self.postImage.addGestureRecognizer(swipeDown)
    }
//
//    @available(iOS 13.0, *)
//    @IBAction func descripExpanded(_ sender: Any) {
//        if self.descripBox.isHidden == true{
//            self.descripBox.isHidden = false
//            self.descripToggle.setImage(UIImage(systemName: "chevron.up"), for: .normal)
//
//        }else{
//            self.descripBox.isHidden = true
//            self.descripToggle.setImage(UIImage(systemName: "chevron.down"), for: .normal)
//
//        }
//    }
    
   
    
    @objc func imageTapped(sender: UITapGestureRecognizer){
           if viewTag.isHidden == true{
               viewTag.isHidden = false
                self.descripBox.isHidden = false
           }else{
               viewTag.isHidden = true
               self.descripBox.isHidden = false
           }
       }
     
    
}
