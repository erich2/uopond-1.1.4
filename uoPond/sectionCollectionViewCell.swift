//
//  sectionCollectionViewCell.swift
//  uoPond
//
//  Created by Ethan Richards on 2/4/20.
//  Copyright © 2020 Ethan Richards. All rights reserved.
//

import UIKit

class sectionCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imagePost: UIImageView!
    
    @IBOutlet weak var senderButton: UIButton!
    
    
    override func awakeFromNib() {
        print("size:", self.contentView.frame.size)
        self.contentView.layer.cornerRadius = 10
    }
}

