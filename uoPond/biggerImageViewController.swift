//
//  biggerImageViewController.swift
//  uoPond
//
//  Created by Ethan Richards on 2/9/20.
//  Copyright © 2020 Ethan Richards. All rights reserved.
//

import UIKit

class biggerImageViewController: UIViewController {

    @IBOutlet weak var imagePost: UIImageView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        showAnimate()
    }
    
    
    
    func showAnimate(){
           self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
           self.view.alpha = 0.0;
           UIView.animate(withDuration: 0.25) {
               self.view.alpha = 1.0
               self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
           }
       }

   
    
}
