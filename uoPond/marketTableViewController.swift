//
//  marketTableViewController.swift
//  uoPond
//
//  Created by Ethan Richards on 5/21/19.
//  Copyright © 2019 Ethan Richards. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseStorage
import FirebaseAuth




class marketTableViewController: UITableViewController, UISearchBarDelegate {
    
   
    
    
    @IBOutlet weak var searchBAR: UISearchBar!
    
    var marketPosts = [Post]()
    var marketImage = [UIImage]()
    //typealias functionFinished = () -> ()
    var refresh: UIRefreshControl!
    var isSearching = false
    var searchPost = [Post]()
    var userTitle = [String]()
    var uid = [String]()
    var myID:String!
    var myName:String?
    var profImage:UIImage?
    var clickedPath: IndexPath?
    var venmo = [String]()
    var descripHeights = [CGFloat]()
    var blacklist = [String]()
    var color:UIColor!
    var emote:UIImage!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.setUpNaviBar()
        self.tableView.backgroundColor = UIColor.black
        searchBAR.delegate = self
        let refresh = UIRefreshControl()
        refresh.tintColor = UIColor.black
        tableView.addSubview(refresh)
        tableView.delegate = self
        DispatchQueue.global(qos: .background).async {
            self.gatherPosts(completion: { () -> Void in
                for u in 0..<self.marketPosts.count{
                    let user = self.marketPosts[u].user
                    self.addVenmos(user: user)
                    print("venmos:", self.venmo)
                }
                self.tableView.reloadData()
            })
        }
        addDoneButton()
        print("Did load:",self.marketPosts.count)
        self.searchBAR.frame.size.height = 0
        self.searchBAR.searchTextField.textColor = UIColor.white
        getToolbar()
        
        
    }
    
 
    func setUpNaviBar(){
        self.navigationController?.navigationBar.backgroundColor = self.color
        self.navigationController?.navigationBar.barTintColor = self.color
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.isToolbarHidden = false
        //self.navigationController?.navigationBar.tintColor = UIColor.white
        self.tableView.backgroundColor = UIColor.white
        //self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.font:UIColor.white]
        self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "AmericanTypewriter", size: 25)!, NSAttributedString.Key.foregroundColor : UIColor.white]
        self.navigationItem.hidesBackButton = true
        let emote = UIBarButtonItem(image: self.emote, style: .done, target: self, action: nil)
        emote.tintColor = UIColor.white
        self.navigationItem.rightBarButtonItem = emote
        self.searchBAR.barTintColor = UIColor.black
    }
    
    @objc func backPressed(sender: UIBarButtonItem){
        let VC = self.storyboard!.instantiateViewController(withIdentifier: "collectionSB") as! marketBiggerTableViewController
        
        VC.data = []
        DispatchQueue.main.asyncAfter(deadline: .now()+0.2) {
            VC.data = ["furniture", "notes", "tech", "clothing", "books"]
            VC.tableView.reloadData()
        }
        
        
        self.navigationController?.pushViewController(VC, animated: false)
    }
 
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isToolbarHidden = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.isToolbarHidden = false
    }
    
    
    func getToolbar(){
        if #available(iOS 13.0, *) {
            let searchBarButton = UIBarButtonItem(image: UIImage(systemName: "magnifyingglass"), style: .plain, target: self, action: #selector(searchTapped(_:)))
//            let profile = UIBarButtonItem(image: UIImage(systemName: "person.circle.fill"), style: .plain, target: self, action: #selector(profButtonPressed(_:)))
            let newBackButton = UIBarButtonItem(image: UIImage(systemName: "return"), style: .plain, target: self, action: #selector(backPressed(sender:)))
            let rightSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
//            let leftSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
            self.toolbarItems = [newBackButton,  rightSpace, searchBarButton]
            self.navigationController?.toolbar.barTintColor = UIColor.black
        } else {
            // Fallback on earlier versions
        }
    }
    
    
    func addDoneButton(){
        let keyboardToolBar = UIToolbar()
        keyboardToolBar.sizeToFit()
         
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem:
            UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem:
            UIBarButtonSystemItem.done, target: self, action: #selector(self.doneClicked) )
         
        keyboardToolBar.setItems([flexibleSpace, doneButton], animated: true)
         
        searchBAR.inputAccessoryView = keyboardToolBar
    }
    
    @objc func doneClicked(){
        view.endEditing(true)
    }

    
    
    @objc func profButtonPressed(_ sender: Any) {
        //self.performSegue(withIdentifier: "toProf", sender: self)
        /*let VC = self.storyboard!.instantiateViewController(withIdentifier: "homeSB") as! homeViewController
        VC.userUid = self.myID
        VC.gatherMyTitles()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            for title in VC.myTitles{
                VC.gatherMessages(title: title)
            }
        }*/
        self.navigationController?.popViewController(animated: true)
    }
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    
    @objc func searchTapped(_ sender: Any) {
        if searchBAR.frame.size.height == 0{
            UIView.animate(withDuration: 0.5) {
                self.searchBAR.frame.size.height = 44
                self.tableView.reloadData()
            }
        }else{
            UIView.animate(withDuration: 0.5) {
                self.searchBAR.frame.size.height  = 0
                self.tableView.reloadData()
            }
        }
    }
    
    func addVenmos(user:String){
        let cat = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Users")
        cat.observe(.value) { (snapshot) in
            if let snaps = snapshot.children.allObjects as? [DataSnapshot]{
                for child in snaps{
                    if child.key == user{
                        let value = child.value as? NSDictionary
                        let vmo = value?["venmo"] as? String ?? ""
                        self.venmo.append("@\(vmo)")
                    }
                }
            }
        }
    }
    
    
    func gatherPosts(completion: @escaping () -> Void?){
        let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Posts")
        ref.observe(.value) { (snapshot) in
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot]{
                for child in snapshots{
                    let value = child.value as? NSDictionary
                    
                    let section = value?["section"] as? String ?? ""
                    if section != nil && section == self.navigationItem.title {
                        let user = value?["user"] as? String ?? ""
                        let title = value?["title"] as? String ?? ""
                        let descrip = value?["description"] as? String ?? ""
                        let price = value?["price"] as? String ?? ""
                        let downloadURL = value?["image\(1)"] as? String ?? ""
                        let userName = value?["userName"] as? String ?? ""
                        let postID = value?["postID"] as? String ?? ""
                        let flagged = value?["flagged"] as? String ?? ""
                        let marketPost = Post(descrip: descrip, title: title, price: price, imageURLs: [downloadURL], user: user, userName: userName, postID: postID)
                        if (self.blacklist.contains(user)) || (flagged == "yes") {
                            print("blocked user")
                        }else{
                            self.marketPosts += [marketPost]
                        }
                        
                    }

                }
                completion()
                }
            else{
                print("ERROR")
            }
            }
        
        print("after function:",self.marketPosts.count)
    }
    
    
    @IBAction func toHome(_ sender: Any) {
        self.performSegue(withIdentifier: "marketToHome", sender: self)
    }

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        if isSearching{
            return searchPost.count
        }else{
            return marketPosts.count
        }
    }
    
    // MARK: - search bar

    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        if searchBar.text == nil || searchBar.text == ""{
            isSearching = false
            tableView.endEditing(true)
            tableView.reloadData()
        }else{
            isSearching = true
            print("searching......")
            searchPost = marketPosts.filter({$0.title.uppercased().contains(searchText.uppercased())} )
            tableView.reloadData()
        }
    }
    
    
    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        var device: Post!
        if isSearching {
            device = self.searchPost[indexPath.row]
        }else{
            device = self.marketPosts[indexPath.row]
        }
        let cell = tableView.dequeueReusableCell(withIdentifier: "marketCell", for: indexPath) as! marketTableViewCell
        let downloadURL = device.imageURLs
        let storageRef = Storage.storage().reference(forURL: downloadURL[0])
        storageRef.getData(maxSize: 20000000, completion: { (data, error) in
            if error != nil{
                print("FIND ME RIGHT HERE:", error!)
                return
            }else{
                let postImage = UIImage(data: data!)
                cell.postImage.image = postImage
            }
        })
        let user = device.user
        let profPicRef = Storage.storage().reference(withPath: "/users/\(user)/profPicture")
        profPicRef.getData(maxSize: 20000000) { (data, err) in
            if err != nil {
                print("uh oh error occured")
                cell.actualUserImage.image = UIImage(named: "Default-welcomer")
            }else{
                let userImage = UIImage(data:data!)
                cell.actualUserImage.image = userImage
            }
        }
        cell.descripBox.text = device.descrip
        
        cell.titleLabel.text = device.title
        cell.priceLabel.text = device.price
        
        self.userTitle = [device.user]
        cell.messageButton.tag = indexPath.row
        cell.messageButton.addTarget(self, action: #selector(message), for: .touchUpInside)
        
        cell.userImage.tag = indexPath.row
        cell.userImage.addTarget(self, action: #selector(userOptions), for: .touchUpInside)
        
        
        return cell
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toProf"{
            let vc = segue.destination as! UINavigationController
            let target = vc.topViewController as! homeViewController
            target.userUid = self.myID!
        }
    }
    
    
    
    
    @objc func message(sender:Any?){
        let button = sender as! UIButton
        let path = button.tag
        if MyVariables.userID != nil{
            if self.marketPosts[path].user == MyVariables.userID{
                   
                    print("can't message myself")
            
                }else{
                    
                    let VC = self.storyboard!.instantiateViewController(withIdentifier: "contactSB") as! chatViewController
                    print("THE PATH:",path)
                    VC.myName = self.myName
                    VC.myID = self.myID
                    VC.receiverName = self.marketPosts[(path)].userName
                    print("USERNAME:",self.marketPosts[path].userName)
                    VC.offerTitle = self.marketPosts[(path)].title
                    VC.receiverID = self.marketPosts[(path)].user
                    VC.senderId = MyVariables.userID
                    
                    VC.navigationItem.title = self.marketPosts[(path)].userName
                    
                    if self.venmo[path] != nil{
                         VC.navigationItem.prompt = self.venmo[path]
                    }
                guard VC.senderId == MyVariables.userID as? String else {
                        
                        print("no sender id")
                        return
                    }
                    self.navigationController?.pushViewController(VC, animated: true)
                }
        }else {
            print("err in id")
            return
        }
        
    }
    
    @objc func userOptions(sender:Any?){
        let button = sender as! UIButton
        let path = button.tag
        
        let optionMenu = UIAlertController(title: nil, message: "Questionable Content?", preferredStyle: UIAlertControllerStyle.actionSheet)
            let blockUser = UIAlertAction(title: "Block User", style: UIAlertActionStyle.default, handler: { (alert: UIAlertAction!) -> Void in
                print("blocking..")
                let key = "no\(UUID().uuidString)"
                let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Users").child("\(String(describing: self.myID!))")
                ref.child("blackList").setValue([key:"\(self.marketPosts[path].user)"]) { (err, ref) in
                    if err != nil {
                        print("err")
                    }else{
                        print("blocked")
                        self.venmo.remove(at: path)
                        self.marketPosts.remove(at: path)
                        self.tableView.deleteRows(at: [IndexPath(row: path, section: 0)], with: .automatic)
                    }
                }
                
            })
            let flagPost = UIAlertAction(title: "Flag Post", style: UIAlertActionStyle.default, handler: {(alert: UIAlertAction!) -> Void in
                print("flagging..")
                let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Posts").child("Post\(self.marketPosts[path].postID)")
                ref.updateChildValues(["flagged":"yes"], withCompletionBlock: { (err, ref) in
                    if err != nil {
                         print("error in flag")
                    }else {
                        print("flagged")
                        self.venmo.remove(at: path)
                        self.marketPosts.remove(at: path)
                        self.tableView.beginUpdates()
                        self.tableView.deleteRows(at: [IndexPath(row: path, section: 0)], with: .automatic)
                        self.tableView.endUpdates()
                    }
                })
            })
            let cancelOption = UIAlertAction(title: "Dismiss", style: UIAlertActionStyle.cancel, handler: {(alert: UIAlertAction!) -> Void in
                print("Cancel")
                optionMenu.dismiss(animated: true, completion: nil)
            })
            optionMenu.addAction(blockUser)
            optionMenu.addAction(flagPost)
            optionMenu.addAction(cancelOption)
            
            present(optionMenu, animated: true, completion: nil)
        }
        
    
    
 
    
 /*   override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         let path = tableView.indexPathForSelectedRow
        let VC = self.storyboard!.instantiateViewController(withIdentifier: "chatSB") as! messagingViewController
        self.navigationController?.pushViewController(VC, animated: true)
        VC.myName = self.myName
        VC.myID = self.myID
        VC.receiverName = self.marketPosts[(path?.row)!].userName
        VC.offerTitle = self.marketPosts[(path?.row)!].title
        VC.receiverID = self.marketPosts[(path?.row)!].user
        VC.navigationItem.title = self.marketPosts[(path?.row)!].userName
    }
    */
    
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCellEditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}


