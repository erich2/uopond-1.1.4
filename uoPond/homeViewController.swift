//
//  homeViewController.swift
//  uoPond
//
//  Created by Ethan Richards on 4/22/19.
//  Copyright © 2019 Ethan Richards. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseStorage
import FirebaseAuth
import FirebaseFunctions
import FirebaseFirestore


class homeViewController: UIViewController, UIImagePickerControllerDelegate, UINavigationControllerDelegate {
 
    

    
    @IBOutlet weak var userNameLabel: UILabel!
    @IBOutlet weak var settingsTab: UIBarButtonItem!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var profilePic: UIImageView!
    
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var inboxContainer: UIView!
    @IBOutlet weak var accountType: UILabel!
    @IBOutlet weak var profView: UIView!
    @IBOutlet weak var trashCan: UIButton!
    
    //@IBOutlet weak var editButton: UIButton!
    @IBOutlet weak var totalMoney: UILabel!
    @IBOutlet weak var totalItems: UILabel!
    @IBOutlet weak var naviView: UIView!
    
    
   
    lazy var functions = Functions.functions()
    var userUid:String?
    var marketPosts = [homePost]()
    var marketImage = [UIImage]()
    typealias functionFinished = () -> ()
    var first:String?
    var postTitles = [String]()
    var settingsShowing = false
    var email = [String]()
    var offerTitles = [String]()
    var ifRead = [String]()
    var finalMess = [Message]()
    var displayMess = [Message]()
    var Rows:Int = 0
    var myTitles = [String]()
    var selected = [Bool]()
    var postIDS = [String]()
    var blacklist = [String]()
    var sections = [String]()
    var times = [String]()
    var finalTitles = [String]()
    var customerID:String!
    var db: Firestore!
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.marketImage = []
        self.marketPosts = []
        MyVariables.userID = Auth.auth().currentUser!.uid
        self.userUid = Auth.auth().currentUser?.uid
        collectionView.delegate = self
        collectionView.dataSource = self
        self.gatherData(completion: { () -> Void? in
            view.reloadInputViews()
        })
        self.gatherPosts(completion: { () -> Void in
            self.collectionView.setNeedsDisplay()
            homeData.posts = self.marketPosts
        })
        self.gatherMyTitles(completion: {
            self.inboxData()
        })
//        self.inboxButton.isUserInteractionEnabled = false
        separateConvos()
        self.inboxData()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            self.totalMoney.text = self.getFormattedCurrency(number: self.getTotalCost())
            print("in the total money")
            //self.collectionView.setNeedsDisplay()
            self.setUpSelection()
            self.inboxData()
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
//            self.inboxButton.isUserInteractionEnabled = true
            self.inboxData()

        }
        db = Firestore.firestore()
        setUpView()
        addDoneButton()
        setUpSelection()
        setUpNav()
    }
    
    
    func inboxData(){
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
            //print("in dispatch",self.myTitles)
                for title in self.myTitles{
                        self.gatherMessages(title: title)
                    print(self.myTitles)
              //          print("offer titles:",self.offerTitles)
                        self.newMessage(title: title)
                        for mess in 0..<self.ifRead.count{
                        if self.ifRead[mess] == "no"{
                                if #available(iOS 13.0, *) {
//                                self.inboxButton.setImage(UIImage(systemName: "envelope.badge.fill"), for: .normal)
                                break
                                   }else {
                                    // Fallback on earlier versions
                                }
                            }
                        }
                    }
                
            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                self.separateConvos()
                print(self.displayMess)
            }
        }
    }
    
    func setUpView(){
//        self.trashCan.layer.borderColor = UIColor.lightGray.cgColor
//        self.trashCan.layer.borderWidth = 1
        self.collectionView.allowsMultipleSelection = true
        self.totalMoney.adjustsFontSizeToFitWidth = true
        self.totalItems.adjustsFontSizeToFitWidth = true
        self.profilePic.layer.cornerRadius = self.profilePic.frame.size.width / 2
        self.profilePic.clipsToBounds = true
        self.view.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        self.trashCan.layer.cornerRadius = self.trashCan.frame.size.width / 2
        //tableView.rowHeight = 100
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapped(sender:)))
        self.profilePic.addGestureRecognizer(tap)
        self.profilePic.isUserInteractionEnabled = true
        self.profView.layer.cornerRadius = 20
        self.accountType.text = "Personal"
        self.naviView.addBottomBorder(UIColor.lightGray, height: 1)
        self.nameLabel.addBottomBorder(UIColor.lightGray, height: 1)
        self.userNameLabel.addBottomBorder(UIColor.lightGray, height: 1)
        setUpTrash()
        let flowLayout = UICollectionViewFlowLayout()
        //self.collectionViewRight.constant = 8
        let size = UIScreen.main.bounds.width / 2 - 10
        flowLayout.itemSize = CGSize(width: size, height: size)
        flowLayout.scrollDirection = .vertical
        flowLayout.minimumInteritemSpacing = 1.0
        flowLayout.minimumLineSpacing = 10
        flowLayout.sectionInset = .init(top: 10, left: 5, bottom: 10, right: 5)
        self.collectionView.collectionViewLayout = flowLayout
        self.collectionView.backgroundColor = UIColor.black.withAlphaComponent(0.7)
        let tap3 = UITapGestureRecognizer(target: self, action: #selector(testFunctions))
        self.nameLabel.addGestureRecognizer(tap3)
        self.nameLabel.isUserInteractionEnabled = true
       // self.inboxButton.applyGradient(colors: [pondColor.color.cgColor, pondColor.secondColor.cgColor])
        setUpInboxColor()
    }
    
    
    func setUpInboxColor(){
        //var gradientView = UIView(frame: CGRect(x: 0, y: 0, width: self.inboxColorView.frame.width, height: self.inboxColorView.frame.height))
         let gradientLayer:CAGradientLayer = CAGradientLayer()
         gradientLayer.colors =
            [pondColor.color.cgColor,pondColor.secondColor.cgColor]
        //Use diffrent colors
    }
    
    @objc func testFunctions(){
        let data = ["text" : "2"]
        functions.httpsCallable("helloWorldFromIOS").call(data) { (result, error) in
            print("in function")
            if let err = error {
                print(err)
            }
            if let res = result?.data {
                print(res)
            }
        }
    }

 
    
    func setUpTrash(){

        trashCan.layer.shadowOpacity = 0.7
        trashCan.layer.shadowRadius = 5.0
        trashCan.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        trashCan.layer.shadowColor = UIColor.lightGray.cgColor
        let panGesture = UIPanGestureRecognizer(target: self, action: #selector(self.panGestureHandler(_:)))
        trashCan.addGestureRecognizer(panGesture)
    }
    

    @objc func panGestureHandler(_ recognizer: UIPanGestureRecognizer){
        self.view.bringSubview(toFront: self.trashCan)
        let translation = recognizer.translation(in: self.view)
        self.trashCan.center = CGPoint(x: self.trashCan.center.x + translation.x, y: self.trashCan.center.y + translation.y)
        recognizer.setTranslation(CGPoint.zero, in: self.view)
    }
    
    func setUpNav(){
        
        self.navigationController?.isNavigationBarHidden = false
        if #available(iOS 13.0, *) {
            let profile = UIBarButtonItem(image: UIImage(systemName: "person.circle.fill"), style: .plain, target: self, action: nil)
            profile.tintColor = UIColor.systemGray
            let marketButton = UIBarButtonItem(image: UIImage(systemName: "globe"), style: .plain, target: self, action: #selector(marketPressed))
            marketButton.tintColor = pondColor.color
            let addPost = UIBarButtonItem(title: "+", style: .plain, target: self, action: #selector(writePost(_:)))
            addPost.setTitleTextAttributes([NSAttributedStringKey.font :UIFont.boldSystemFont(ofSize: 30), NSAttributedStringKey.foregroundColor : pondColor.color], for: .normal)
            let inbox = UIBarButtonItem(image: UIImage(systemName: "envelope"), style: .plain, target: self, action: #selector(openInbox))
            inbox.tintColor = pondColor.color
            let rightSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
            let leftSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
            self.navigationController?.toolbar.isTranslucent = true
            self.navigationController?.toolbar.barTintColor = UIColor.black
            self.setToolbarItems([ inbox, rightSpace, addPost, leftSpace, marketButton], animated: true)
        } else {
            // Fallback on earlier versions
        }
        self.navigationItem.title = ""
        self.navigationItem.hidesBackButton = true
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController!.navigationBar.titleTextAttributes = [NSAttributedStringKey.font: UIFont(name: "Symbol", size: 25)!,NSAttributedStringKey.foregroundColor : UIColor.white]
        self.navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationController?.isToolbarHidden = false
        self.settingsTab.tintColor = pondColor.color
    }
    
    
    func regroup(){
//        self.marketPosts = []
//        self.marketImage = []
//        self.gatherPosts(completion: { () -> Void in
//            self.tableView.reloadData()
//            self.totalMoney.text = self.getFormattedCurrency(number: self.getTotalCost())
//        })
        self.collectionView.reloadData()
        setUpSelection()
        
    }
    

    
    func setUpSelection(){
        if self.selected.count != self.marketPosts.count {
            self.selected = []
            for val in 0..<self.marketPosts.count {
                    self.selected.append(false)
                }
            }
        }
    
    
    override func viewDidAppear(_ animated: Bool) {
        self.gatherData { () -> Void? in
            view.reloadInputViews()
        }
        self.finalMess = []
        self.displayMess = []
        self.offerTitles = []
//        self.marketPosts = []
//        self.marketImage = []
        self.selected = []
        for title in self.myTitles{
            self.gatherMessages(title: title)
            self.newMessage(title: title)
        }
       
        regroup()
        if homeData.image != nil {
            self.profilePic.image = homeData.image
        }
    }
    
    func addDoneButton(){
        let keyboardToolBar = UIToolbar()
        keyboardToolBar.sizeToFit()
         
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem:
            UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem:
            UIBarButtonSystemItem.done, target: self, action: #selector(self.doneClicked) )
         
        keyboardToolBar.setItems([flexibleSpace, doneButton], animated: true)
         
        
    }
    
    @objc func doneClicked(){
        view.endEditing(true)
    }
    

    @IBAction func settingsClicked(_ sender: Any) {
       
          //initiate pop up view
        let VC = self.storyboard!.instantiateViewController(withIdentifier: "settingsTabSB") as! settingsTabViewController
        //VC.profilePic.image = self.profilePic.image
        VC.userUid = self.userUid
        self.navigationController?.pushViewController(VC, animated: false)
        
    }
    
    @IBAction func trashCanRows(_ sender: Any) {
        if self.trashCan.tintColor == UIColor.black {
            return
        }
        if self.trashCan.tintColor == UIColor.systemRed{
            for val in 0..<self.selected.count {
                print(self.selected)
                if self.selected[val] == true{
                    print(val, "--> index")
                    let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/").child("Posts")
                    ref.child("Post\(self.marketPosts[val].postID)").removeValue()
                    
                    DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
                        
                        self.marketPosts.remove(at: val)
                        self.selected.remove(at: val)
                        self.collectionView.reloadData()
                    }
                    
                   
                }
            }
            self.trashCan.tintColor = UIColor.black
        }
    }
    
    
    @objc func tapped(sender: UIGestureRecognizer){
        let picker = UIImagePickerController()
        picker.sourceType = UIImagePickerControllerSourceType.photoLibrary
        picker.allowsEditing = true
        picker.delegate = self
        present(picker, animated: true, completion: nil)
    }
    

 
    func gatherMyTitles(completion: () -> Void?){
    let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Posts")
    ref.observe(.value) { (snapshot) in
        if let snapshots = snapshot.children.allObjects as? [DataSnapshot]{
            for child in snapshots{
                let value = child.value as? NSDictionary
                let title = value?["title"] as? String ?? ""
                self.myTitles.append(title)
                }
            }
        }
        completion()
    }
    
//    ADD A MIDDLE DESIGN PEICE
    
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
           
           var selectedImage: UIImage?
           if let editedImage = info["UIImagePickerControllerEditedImage"] as? UIImage{
               selectedImage = editedImage
           }else if let originalImage = info["UIImagePickerControllerOriginalImage"] as? UIImage {
               selectedImage = originalImage
           }
           self.profilePic.image = selectedImage
            homeData.image = selectedImage
            guard let image = selectedImage else{
                print("error")
                return
            }
            let imageName = UUID().uuidString
            let data = UIImageJPEGRepresentation(image, 1.0)
            let imageRef = Storage.storage().reference().child("users").child("\(self.userUid!)").child("profPicture.jpg")
            imageRef.putData(data!, metadata: nil) { (metadata, error) in
                if error != nil {
                    print(error!)
                    return
                }else{
                    imageRef.downloadURL(completion: { (url, err) in
                        if err != nil{
                            print(err!)
                            return
                        }else{
                            guard let url = url else{
                                print("unsuccessful")
                                return
                            }
                            let urlString = url.absoluteString
                            let uid = self.userUid!
                            let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Users")
                            let userReference = ref.child(uid)
                            let values = ["profilePicture" : urlString]
                            userReference.updateChildValues(values, withCompletionBlock: { (err, ref) in
                                if err != nil{
                                    print(err!)
                                    return
                                }else{
                                    print("success")
                                }
                            })
                        }
                    })
                }
                
            }
        
           picker.dismiss(animated: true, completion: nil)
       }
       
       
       func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        
           picker.dismiss(animated: true, completion: nil)
       }
    
    
 
    

    

        @objc func openInbox() {
        
        let VC = self.storyboard!.instantiateViewController(withIdentifier: "inboxSB") as! inboxTableViewController
        //self.navigationController?.pushViewController(VC, animated: false)
        VC.myID = self.userUid!
        VC.postTitles = self.myTitles
        VC.myName = self.nameLabel.text
        VC.finalMess = self.finalMess
        DisplayMess.mess = self.displayMess
        VC.displayMess = DisplayMess.mess
        VC.offerTitles = self.finalTitles
        VC.ifRead = self.ifRead
        VC.Rows = self.Rows
        if self.finalMess.isEmpty == false {
            if self.displayMess.isEmpty == true {
                print("error gathering")
                self.inboxData()
                return
            }
        }
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.3) {
            self.navigationController?.pushViewController(VC, animated: false)
        }
//        VC.modalPresentationStyle = .popover
//
//        self.present(VC, animated: true, completion: nil)
    }
    
    func gatherData(completion: () -> Void?){
        let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Users")
        ref.child("\(self.userUid!)/").observe( .value) { (snapshot) in
                if let value = snapshot.value as? [String:Any] {
            
            //guard let value = snapshot.children.allObjects as! NSDictionary else { print("ERROR"); return }
                    let currentFirst = value["firstName"] as? String ?? ""
                    let currentLast = value["lastName"] as? String ?? ""
//                    let vName = value["venmo"] as? String ?? ""
                    let fullname = currentFirst + " " + currentLast
                    self.nameLabel.text = fullname
                    let username = value["username"] as? String ?? ""
                    self.navigationItem.title = "profile"
                    self.userNameLabel.text = username
                    let downloadURL = value["profilePicture"] as? String ?? ""
                    let storageRef = Storage.storage().reference(forURL: downloadURL)
                    storageRef.getData(maxSize: 20000000, completion: { (data, error) in
                        if error != nil{
                            print("error gettin image in gatherdata")
                        }else{
                            let postImage = UIImage(data: data!)
                            self.profilePic.startAnimating()
                            self.profilePic.image = postImage
                            homeData.image = postImage!
                            self.profilePic.stopAnimating()
                        }
                    })
                }else{
                    print("Not creating dictionary")
                }
        }
        let cast = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Users").child("\(self.userUid!)")
        cast.child("blackList").observe(.value) { (snap) in
            if let value = snap.value as? [String:String] {
                self.blacklist = Array(value.values)
            }
        }
        completion()
    }

    func newMessage(title:String){
        let query = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Messages/\(title)")
        query.observe(.value) { (snapshot) in
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot]{
                for child in snapshots{
                    let key = child.key
                    if key.contains("\(self.userUid!)") {
                        let cast = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Messages/\(title)").child(key)
                        cast.observe(.value) { (snap) in
                            if let snapshots = snap.children.allObjects as? [DataSnapshot]{
                                for child in snapshots{
                                    if child.key.contains("-read"){
                                        if child.key == "\(self.userUid!)-read"{
                                            if (child.value as! String) == "no" {
//                                                self.inboxButton.titleLabel?.text = "!"
                                            }
                                        }
                                    }
                                }
                                
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    func gatherMessages(title : String){
             let query = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Messages/\(title)")
             query.observe(.value) { (snapshot) in
                 if let snapshots = snapshot.children.allObjects as? [DataSnapshot]{
                     for child in snapshots{
                         let key = child.key
                         if key.contains("\(self.userUid!)") {
                             let cast = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Messages/\(title)").child(key)
                             cast.observe(.value) { (snap) in
                                 if let snapshots = snap.children.allObjects as? [DataSnapshot]{
                                     for child in snapshots{
                                         if child.key.contains("-read"){
                                             if child.key == "\(self.userUid!)-read"{
                                                 self.ifRead.append(child.value as! String)
                                             }else{
                                                 self.ifRead.append("yes")
                                             }
                                         }else{
                                             let data = child.value as? NSDictionary
                                             let id = data?["sender_id"] as? String ?? ""
                                             let time = data?["time"] as? String ?? ""
                                             let text = data?["text"] as? String ?? ""
                                             let receiver = data?["receiverID"] as? String ?? ""
                                             let offerTitle = data?["title"] as? String ?? ""
                                            let trans = data?["transaction"] as? String ?? ""
                                             if (id == self.userUid!) || (receiver == self.userUid!){
                                                let mess = Message(sender: id, receiver: receiver, timeStamp: time, text: text, transaction: trans)
                                                 self.finalMess.append(mess)
                                                 self.offerTitles.append(offerTitle)
                                                
                                                
                                             }
                                         }
                                     }
                                 }
                             }
                         }
                     }
                 }
             }
     }
     
    func separateConvos(){
        var finalTitles = [String]()
        var conversations = [String:Message]()
        var key = ""
        for title in 0..<self.offerTitles.count {
            //print("offertitles count",self.offerTitles.count)
            if finalTitles.contains(self.offerTitles[title]) {
                //print("already in final titles")
            }else{
                //print("final titles:",finalTitles)
                finalTitles.append(self.offerTitles[title])
            }
        }
        //print("final titles:",finalTitles)
        for title in 0..<finalTitles.count {
            //var tempConvo = [String:Message]()
            for titles in 0..<self.offerTitles.count {
                if self.offerTitles[titles] == finalTitles[title] {
                    key = finalTitles[title]
                    conversations[key] = self.finalMess[titles]
                    
                }
            }
        }
        //print(conversations)
        self.displayMess = Array(conversations.values)
        self.finalTitles = finalTitles
        self.Rows = conversations.keys.count
    }
     
    func organize(messages: [Message]) -> Message{
           
            var displayFinalMess = [String:Message]()
            for mess in 0..<messages.count{
                    if messages[mess].sender == self.userUid{
                    
                        if displayFinalMess[messages[mess].receiver] == nil{
                            displayFinalMess[messages[mess].receiver] = messages[mess]
                        }else if displayFinalMess[messages[mess].receiver] != nil{
                            if (displayFinalMess[messages[mess].receiver]?.timeStamp)! > messages[mess].timeStamp{
                                break
                            }else{
                                displayFinalMess[messages[mess].receiver] = messages[mess]
                            }
                        }
                    }else if messages[mess].receiver == self.userUid{
                        
                        if displayFinalMess[messages[mess].sender] == nil{
                            displayFinalMess[messages[mess].sender] = messages[mess]
                        }else if displayFinalMess[messages[mess].sender] != nil{
                            if (displayFinalMess[messages[mess].sender]?.timeStamp)! > messages[mess].timeStamp{
                                break
                            }else{
                                displayFinalMess[messages[mess].sender] = messages[mess]
                            }
                        }
                    }
                }
            print("in organize func:", Array(displayFinalMess.values))
            return Array(displayFinalMess.values)[0]
     
        }
    
    
  
    @objc func marketPressed(_ sender: Any) {
        let VC = self.storyboard!.instantiateViewController(withIdentifier: "collectionSB") as! marketBiggerTableViewController
        self.navigationController?.pushViewController(VC, animated: false)
        VC.myID = self.userUid!
        VC.myName = self.nameLabel.text
        VC.blacklist = self.blacklist
        VC.myUserName = self.userNameLabel.text
        MyVariables.userName = self.userNameLabel.text!
    }
    
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "toMarket"{
            let vc = segue.destination as! UINavigationController
            let target = vc.topViewController as! marketTableViewController
            target.myID = self.userUid!
            target.myName = self.nameLabel.text
            
        }else if segue.identifier == "signedOut"{
            do{
                try Auth.auth().signOut()
                self.removeSpinner()
            }catch{
                print("error signing out")
                self.removeSpinner()
            }
        }else if segue.identifier == "newPost" {
            let vc = segue.destination as! UINavigationController
            let target = vc.topViewController as! popUpViewController
            target.ownUserName = [(self.userNameLabel.text!)]
        }
    }
    
    func gatherPosts(completion: () -> Void?){
        self.sections = []
        let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/")
        ref.child("Posts").observe(.value) { (snapshot) in
            if let snapshots = snapshot.children.allObjects as? [DataSnapshot]{
                for child in snapshots{
                    let value = child.value as? NSDictionary
                    // if statement making sure user posted it
                    let user = value?["user"] as? String ?? ""
                    if user == self.userUid! {
                        //var urls = [String]()
                        let title = value?["title"] as? String ?? ""
                        let descrip = value?["description"] as? String ?? ""
                        let price = value?["price"] as? String ?? ""
                        let user = value?["user"] as? String ?? ""
                        //let imgNumber = value?["imgNumber"] as? String ?? ""
//                        for img in 0..<Int(imgNumber)! {
//                            let image = value?["image\(img)"] as? String ?? ""
//                            urls.append(image)
//                        }
                        let image = value?["image0"] as? String ?? ""
                        let userName = value?["userName"] as? String ?? ""
                        let postID = value?["postID"] as? String ?? ""
                        let category = value?["section"] as? String ?? ""
                        let postTime = value?["time"] as? String ?? ""
                        self.sections.append(category)
                        /*
                        let storageRef = Storage.storage().reference(forURL: downloadURL)
                        storageRef.getData(maxSize: 20000000, completion: { (data, error) in
                            if error != nil{
                                print("FIND ME RIGHT HERE:", error!)
                                return
                            }else{
                                let image = UIImage(data: data!)
                                let postImage = image
                                self.tableView.reloadData()
                            }
                        })
                        */
                        let post = homePost(descrip: descrip, title: title, price: price, imageURLs: image, user: user, userName: userName, postID: postID)
                        
                        if self.postIDS.contains(postID) {
                //            print("already have post")
                        }else{
              //              print("adding post to view")
                            self.marketPosts += [post]
                            self.postTitles += [title]
                            self.selected.append(false)
                            self.postIDS += [postID]
                            self.times += [postTime]
                            self.totalMoney.text = self.getFormattedCurrency(number: self.getTotalCost())
                        }
                        
                    }else{
                        print("error")
                    }
                    }
            //    print("Success")
          //      print("after success:",self.marketPosts.count)
                self.collectionView.reloadData()
            }
        }
        completion()
        //nil right here for some reason
        //print("at end of function:", self.marketPosts.count)
        //print("at the end of the function:", self.selected.count)
    }
 
    
    func getTotalCost() -> Int{
        var total = 0
        for price in 0..<self.marketPosts.count{
            let price = self.marketPosts[price].price
            let amount = Int(price.removeFormatAmount())
            if amount != nil {
                total += amount
            }
        }
       // print("total",total)
        return total
    }
 
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        
    }
    
    func welcomeUser(){
        let welcome = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "welcomeSB") as! welcomeViewController
        self.addChildViewController(welcome)
        welcome.view.frame = self.view.frame
        self.view.addSubview(welcome.view)


    }
    
    
    @objc func writePost(_ sender: Any) {
        
        self.performSegue(withIdentifier: "newPost", sender: self)
       
    }
    

}
extension homeViewController: UICollectionViewDelegate, UICollectionViewDataSource{
    
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        var result = 1
        if self.marketPosts.count == 0{
            self.totalItems.text = "0 items"
            self.totalMoney.text = self.getFormattedCurrency(number: 0)
            result = 1
        }else{
            if self.marketPosts.count == 1{
                self.totalItems.text = "1 item:"
            }else{
                self.totalItems.text = "\(self.marketPosts.count) items:"
            }
            result = self.marketPosts.count
        }
        return result
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        //let cell = collectionView.dequeueReusableCell(withIdentifier: "homeCell", for: indexPath) as! homeTableViewCell
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "homeCell", for: indexPath) as! homeCollectionViewCell

        if self.marketPosts.count == 0{
            cell.titleLabel.text = "No Posts:("
            cell.priceLabel.isHidden = true
            cell.thumbNail.isHidden = true
            cell.contentView.layer.borderWidth = 0
            cell.categoryEmote.isHidden = true
            cell.expiration.isHidden = true
//            cell.plusLabel.isHidden = true
            self.collectionView.allowsSelection = false
        }else{
            cell.titleLabel.adjustsFontSizeToFitWidth = true
            cell.priceLabel.adjustsFontSizeToFitWidth = true
            self.collectionView.allowsSelection = true
            cell.expiration.isHidden = false
//            cell.plusLabel.isHidden = false
            if var diff:Int = 0 {
                diff = Int(Date().timeIntervalSinceReferenceDate) - ((self.times[indexPath.row]) as NSString).integerValue
                diff = diff / 86400
                diff = 30 - diff
                let fin = String(describing: diff)
                cell.expiration.text = "\(fin) days"
                cell.expiration.adjustsFontSizeToFitWidth = true
            }
            
            cell.categoryEmote.isHidden = false
            cell.thumbNail.isHidden = false
            cell.priceLabel.isHidden = false
            cell.titleLabel.text = self.marketPosts[indexPath.row].title
            cell.priceLabel.text = self.marketPosts[indexPath.row].price
            //let number = self.marketPosts[indexPath.row].imageURLs.count
            DispatchQueue.main.async(execute: {
                let storageRef = Storage.storage().reference(forURL: self.marketPosts[indexPath.row].imageURLs)
                storageRef.getData(maxSize: 20000000, completion: { (data, error) in
                    if error != nil{
                        print("FIND ME RIGHT HERE:", error!)
                        return
                    }else{
                        let postImage = UIImage(data: data!)
                        cell.thumbNail.image = postImage
                        self.collectionView.setNeedsDisplay()
                    }
                })
            })
           
//            if number > 1 {
//                let storageRef2 = Storage.storage().reference(forURL: self.marketPosts[indexPath.row].imageURLs[1])
//                storageRef2.getData(maxSize: 20000000, completion: { (data, error) in
//                    if error != nil{
//                        print("FIND ME RIGHT HERE:", error!)
//                        return
//                    }else{
//                        let postImage = UIImage(data: data!)
////                        cell.thumbNail2.image = postImage
////                        cell.thumbNail2.isHidden = false
//                        //cell.thumbNail.alpha = 0.5
//                        self.collectionView.reloadData()
//                    }
//                })
//            }
//            if number > 2{
////                cell.thumbNail2.alpha = 0.65
////                cell.plusLabel.isHidden = false
//            }
            
            if #available(iOS 13.0, *) {
                cell.categoryEmote.layer.backgroundColor = self.getBackgroundColor(input: self.sections[indexPath.row]).cgColor
                cell.categoryEmote.setImage(self.getEmote(input: self.sections[indexPath.row]), for: .normal)
                //cell.contentView.addBottomBorder( self.getBackgroundColor(input: self.sections[indexPath.row]), height: 1.2)
            } else {
                // Fallback on earlier versions
            }
        }
        
        if self.selected.count > 0 {
            if self.selected[indexPath.row] == true {
                cell.contentView.layer.borderColor = UIColor.systemBlue.cgColor
                cell.contentView.layer.borderWidth = 2
                cell.contentView.layer.cornerRadius = 10
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        print(" did select at row clicked")
        if self.marketPosts.count > 0{
            if self.selected.count > 0{
                if self.selected[indexPath.row] == false {
                    self.selected[indexPath.row] = true
                    print("selected")
                }else{
                    self.selected[indexPath.row] = false
                    print("Deselected")
                    let cell = self.collectionView.cellForItem(at: indexPath)
                    cell!.contentView.layer.borderWidth = 0
                }
                for val in 0..<self.selected.count {
                    print(self.selected)
                    if self.selected[val] == true{
                        self.trashCan.tintColor = UIColor.systemRed
                        return
                    }
                    self.trashCan.tintColor = UIColor.white
                }
            }else{
                print("selected not greater than 0")
                setUpSelection()
            }
        }else{
            print("market not greater than 0")
        }
    }
    
    func getFormattedCurrency(number: Int) -> String {
        let currencyFormatter = NumberFormatter()
         currencyFormatter.usesGroupingSeparator = true
         currencyFormatter.numberStyle = NumberFormatter.Style.currency
        
         currencyFormatter.locale = NSLocale.current
        let priceString = currencyFormatter.string(from: number as NSNumber)
        return priceString!
        
    }
    
    
    

    
    @available(iOS 13.0, *)
    func getBackgroundColor(input:String) -> UIColor{
            if input == "furniture"{
                let color = UIColor.systemRed.withAlphaComponent(0.90)
                return color
            }
            if input == "notes"{
                let color = UIColor.systemTeal.withAlphaComponent(0.90)
                return color
            }
            if input == "tech"{
                let color = UIColor.systemGreen.withAlphaComponent(0.9)
                return color
            }
            if input == "clothing"{
                let color = UIColor.systemPurple.withAlphaComponent(0.9)
                return color
            }
            if input == "books"{
                let color = UIColor.systemIndigo.withAlphaComponent(0.90)
                return color
            }
        return UIColor.lightGray
    }
    
    @available(iOS 13.0, *)
    func getEmote(input:String) -> UIImage{
            if input == "furniture"{
                let image = UIImage(systemName: "bed.double")!
                return image
            }
            if input == "notes"{
                let image = UIImage(systemName: "square.and.pencil")!
                return image
            }
            if input == "tech"{
                let image = UIImage(systemName: "desktopcomputer")!
                return image
            }
            if input == "clothing"{
                let image = UIImage(systemName: "flame.fill")!
                return image
            }
            if input == "books"{
                let image = UIImage(systemName: "book.fill")!
                return image
            }
        return UIImage(systemName: "bed.double")!
    }
    
    
}
extension String {
    func removeFormatAmount() -> Double {
        let formatter = NumberFormatter()

        formatter.locale = Locale(identifier: "en_US")
        formatter.numberStyle = .currency
        formatter.currencySymbol = "$"
        formatter.decimalSeparator = ","

        return formatter.number(from: self) as! Double? ?? 0
     }
}
struct MyVariables {
    static var userID = "someString"
    static var userName = "string"
}
extension UIView {
    func addBottomBorder(_ color: UIColor, height: CGFloat) {
        let border = UIView()
        border.backgroundColor = color
        border.translatesAutoresizingMaskIntoConstraints = false
        self.addSubview(border)
        border.addConstraint(NSLayoutConstraint(item: border,
            attribute: NSLayoutAttribute.height,
            relatedBy: NSLayoutRelation.equal,
            toItem: nil,
            attribute: NSLayoutAttribute.height,
            multiplier: 1, constant: height))
        self.addConstraint(NSLayoutConstraint(item: border,
            attribute: NSLayoutAttribute.bottom,
            relatedBy: NSLayoutRelation.equal,
            toItem: self,
            attribute: NSLayoutAttribute.bottom,
            multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: border,
            attribute: NSLayoutAttribute.leading,
            relatedBy: NSLayoutRelation.equal,
            toItem: self,
            attribute: NSLayoutAttribute.leading,
            multiplier: 1, constant: 0))
        self.addConstraint(NSLayoutConstraint(item: border,
            attribute: NSLayoutAttribute.trailing,
            relatedBy: NSLayoutRelation.equal,
            toItem: self,
            attribute: NSLayoutAttribute.trailing,
            multiplier: 1, constant: 0))
    }
    func addTopBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: 0, width: self.frame.size.width, height: width)
        self.layer.addSublayer(border)
    }

    func addRightBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: self.frame.size.width - width, y: 0, width: width, height: self.frame.size.height)
        self.layer.addSublayer(border)
    }

    func addLeftBorderWithColor(color: UIColor, width: CGFloat) {
        let border = CALayer()
        border.backgroundColor = color.cgColor
        border.frame = CGRect(x: 0, y: 0, width: width, height: self.frame.size.height)
        self.layer.addSublayer(border)
    }
    
    @discardableResult
    func applyGradient(colours: [UIColor]) -> CAGradientLayer {
        return self.applyGradient(colours: colours, locations: nil)
    }

    @discardableResult
    func applyGradient(colours: [UIColor], locations: [NSNumber]?) -> CAGradientLayer {
        let gradient: CAGradientLayer = CAGradientLayer()
        gradient.frame = self.bounds
        gradient.colors = colours.map { $0.cgColor }
        gradient.locations = locations
        self.layer.insertSublayer(gradient, at: 0)
        return gradient
    }
    
}

struct homeData {
    static var posts = [homePost]()
    static var image:UIImage!
}
struct DisplayMess {
    static var mess = [Message]()
}

struct pondColor {
   // static var color:UIColor = UIColor.init(red: 30/255, green: 215/255, blue: 96/255, alpha: 0.7)
    static var secondColor:UIColor = UIColor.black
    static var color:UIColor = UIColor.systemBlue
}
extension UIButton
{
    func applyGradient(colors: [CGColor])
    {
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = colors
        gradientLayer.startPoint = CGPoint(x: 0, y: 0)
        gradientLayer.endPoint = CGPoint(x: 1, y: 0)
        gradientLayer.frame = self.bounds
        self.layer.insertSublayer(gradientLayer, at: 0)
    }
}
