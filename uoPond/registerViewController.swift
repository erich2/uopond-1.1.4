//
//  registerViewController.swift
//  uoPond
//
//  Created by Ethan Richards on 4/19/19.
//  Copyright © 2019 Ethan Richards. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase

class registerViewController: UIViewController {
    
    @IBOutlet weak var usernameBox: UITextField!
    @IBOutlet weak var emailBox: UITextField!
    @IBOutlet weak var confirmEmailBox: UITextField!
    @IBOutlet weak var passwordBox: UITextField!
    @IBOutlet weak var confirmPasswordBox: UITextField!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var toLoginButton: UIButton!
    @IBOutlet weak var registerSuccess: UIImageView!
    var userUid:String?
    @IBOutlet weak var shadowView: UIView!
    @IBOutlet weak var shadowView2: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapped(sender:)))
        self.view.addGestureRecognizer(tap)
       
        self.view.applyGradient(colours: [UIColor.black, UIColor.clear], locations: [0.0, 0.75, 1.0])
        self.roundCorners(view: self.usernameBox, corners: [.topRight, .topLeft], radius: 10)
        self.roundCorners(view: self.confirmPasswordBox, corners: [.bottomRight, .bottomLeft], radius: 10)
        self.usernameBox.addBottomBorder(UIColor.black.withAlphaComponent(0.4), height: 1)
        self.emailBox.addBottomBorder(UIColor.black.withAlphaComponent(0.4), height: 1)
        self.confirmEmailBox.addBottomBorder(UIColor.black.withAlphaComponent(0.4), height: 1)
        self.passwordBox.addBottomBorder(UIColor.black.withAlphaComponent(0.4), height: 1)
        self.registerButton.addBottomBorder(UIColor.black.withAlphaComponent(0.4), height: 1)
        self.shadowView.addShadowFinal()
        self.roundCorners(view: self.registerButton, corners: [.topRight, .topLeft], radius: 10)
        self.roundCorners(view: self.toLoginButton, corners: [.bottomRight, .bottomLeft], radius: 10)
        //self.confirmPasswordBox.addShadowFinalLonger()
        self.shadowView2.addShadowFinalLonger()
        self.shadowView2.layer.cornerRadius = 10
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
   
    
    @objc func tapped(sender: UIGestureRecognizer) {
           self.view.endEditing(true)
           print("tapped")
       }
    
    
    func roundCorners(view :UIView, corners: UIRectCorner, radius: CGFloat){
             let path = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
             let mask = CAShapeLayer()
             mask.path = path.cgPath
             view.layer.mask = mask
     }
        
    
    @IBAction func signUpAction(_ sender: Any) {
       
        if passwordBox.text != confirmPasswordBox.text {
            let alertController = UIAlertController(title: "Password Incorrect", message: "Please make sure your passwords match", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }
        else if emailBox.text != confirmEmailBox.text{
            let alertController = UIAlertController(title: "Email Incorrect", message: "Please make sure your emails match", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
            }
        else if passwordBox.text == nil || confirmPasswordBox.text == nil || emailBox.text == nil || confirmEmailBox.text == nil || usernameBox.text == nil{
            let alertController = UIAlertController(title: "Oops", message: "Please fill out all fields", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
            
            
        }else if emailBox.text!.contains("@uoregon.edu") || emailBox.text!.contains("the.peter.richards@gmail.com"){
            self.showSpinner(onView: self.shadowView2)
            Auth.auth().createUser(withEmail: emailBox.text!, password: passwordBox.text!) { (user, error) in
                if error == nil{
                    guard let uid = Auth.auth().currentUser?.uid else {
                        print("error in the uid ")
                        return
                    }
                    self.userUid = uid
                    print("BEFoRE the UID",self.userUid!)
                    let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/")
                    let userReference = ref.child("Users").child(uid)
                    let values = ["username" : self.usernameBox.text!, "email" : self.emailBox.text!]
                    userReference.updateChildValues(values, withCompletionBlock: { (err, ref) in
                        if error != nil{
                            print(err!)
                            self.removeSpinner()
                            return
                        }else{
                            print("success!")
                            self.removeSpinner()
                            self.registerButton.isHidden = true
                            self.usernameBox.isHidden = true
                            self.emailBox.isHidden = true
                            self.confirmEmailBox.isHidden = true
                            self.passwordBox.isHidden = true
                            self.confirmPasswordBox.isHidden = true
                            self.registerSuccess.isHidden = false
                            DispatchQueue.main.asyncAfter(deadline: .now() + 0.5) {
                                let VC = self.storyboard!.instantiateViewController(withIdentifier: "profileSB") as! profileViewController
                                self.navigationController?.pushViewController(VC, animated: true)
                                VC.UID = self.userUid
                                VC.email = self.emailBox.text
                            }
                            return
                        }
                    })
             
                }else{
                    self.removeSpinner()
                    let alertController = UIAlertController(title: "Password Incorrect", message: error?.localizedDescription, preferredStyle: .alert)
                    let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                    alertController.addAction(defaultAction)
                    self.present(alertController, animated: true, completion: nil)
                }
            }
        
        }else{
            let alertController = UIAlertController(title: "I'm sorry", message: "pond is only available to UO studetnts.", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }

    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "OneMoreStep"{
            let vc = segue.destination as! profileViewController
            vc.UID = self.userUid
        }
    }
    
    
    
    @IBAction func toLoginPressed(_ sender: Any) {
        let VC = self.storyboard!.instantiateViewController(withIdentifier: "loginSB") as! loginViewController
        self.navigationController?.pushViewController(VC, animated: false)
    }
    

    
}
var vSpinner: UIView?
extension UIViewController{
    func showSpinner(onView: UIView){
        let spinnerView = UIView.init(frame: onView.bounds)
        spinnerView.backgroundColor = UIColor.init(red: 0.5, green: 0.5, blue: 0.5, alpha: 0.5)
        let ai = UIActivityIndicatorView.init(activityIndicatorStyle: .whiteLarge)
        ai.startAnimating()
        ai.center = spinnerView.center
        DispatchQueue.main.async{
            spinnerView.addSubview(ai)
            onView.addSubview(spinnerView)
        }
        vSpinner = spinnerView
    }
    
    func removeSpinner(){
        DispatchQueue.main.async{
            vSpinner?.removeFromSuperview()
            vSpinner = nil
        }
    }
}

