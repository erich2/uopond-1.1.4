//
//  homeTableViewCell.swift
//  uoPond
//
//  Created by Ethan Richards on 4/30/19.
//  Copyright © 2019 Ethan Richards. All rights reserved.
//

import UIKit

class homeTableViewCell: UITableViewCell {

    @IBOutlet weak var categoryEmote: UIButton!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var thumbNail: UIImageView!
    @IBOutlet weak var cellView: UIView!
    @IBOutlet weak var expiration: UILabel!
    @IBOutlet weak var thumbNail2: UIImageView!
    @IBOutlet weak var plusLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.cellView.layer.shadowColor = UIColor.lightGray.cgColor
        self.cellView.layer.cornerRadius = 25
        self.thumbNail.layer.cornerRadius = self.thumbNail.frame.size.width / 2
        self.thumbNail2.layer.cornerRadius = self.thumbNail.frame.size.width / 2
        self.thumbNail.clipsToBounds = true
        self.thumbNail2.clipsToBounds = true
        self.priceLabel.textColor = UIColor.white
        self.titleLabel.textColor = UIColor.white
        self.priceLabel.adjustsFontSizeToFitWidth = true
        self.categoryEmote.layer.cornerRadius = self.categoryEmote.frame.size.width / 2
        self.titleLabel.adjustsFontSizeToFitWidth = true
        self.contentView.layer.shadowOpacity = 0.7
        self.contentView.layer.shadowRadius = 5.0
        self.contentView.layer.shadowOffset = CGSize(width: 0.0, height: 0.0)
        self.contentView.layer.shadowColor = UIColor.lightGray.cgColor
    }
    
   

   
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: false)
        
       
    }

}

extension UIView {

    public func addShadowToView(shadow_color: UIColor,offset: CGSize,shadow_radius: CGFloat,shadow_opacity: Float,corner_radius: CGFloat) {
        self.layer.shadowColor = shadow_color.cgColor
        self.layer.shadowOpacity = shadow_opacity
        self.layer.shadowOffset = offset
        self.layer.shadowRadius = shadow_radius
        self.layer.cornerRadius = corner_radius
    }
}
