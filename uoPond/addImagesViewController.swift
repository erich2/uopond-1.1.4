//
//  addImagesViewController.swift
//  uoPond
//
//  Created by Ethan Richards on 2/27/20.
//  Copyright © 2020 Ethan Richards. All rights reserved.
//

import UIKit

class addImagesViewController: UIViewController {

    
    var picker = UIImagePickerController()
    var post:tempPost!
    var images = [UIImage]()
    var count:Int = 0
    
    @IBOutlet weak var imageView2: UIImageView!
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var countUploaded: UILabel!
    
    @IBOutlet weak var uploadButton: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        picker.delegate = self
        self.countUploaded.text = "0"
        self.addToolbarItems()
        self.countUploaded.textColor = UIColor.lightGray
        self.imageView2.contentMode = .scaleAspectFill
        self.countUploaded.adjustsFontSizeToFitWidth = true
        self.countUploaded.layer.cornerRadius = self.countUploaded.frame.size.width / 2
        self.countUploaded.layer.borderColor = UIColor.black.cgColor
        self.countUploaded.layer.borderWidth = 1
    }
    
    func addToolbarItems(){

        let next = UIBarButtonItem(title: "Preview", style: .plain, target: self, action: #selector(previewPressed))
        let middleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
       
        self.toolbarItems = [middleSpace, next]
        self.navigationController?.isToolbarHidden = false
    }
    
    @objc func previewPressed(){
        if self.images.count == 0  {
            let alertController = UIAlertController(title: "Oops", message: "Please add an image", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }else{
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "previewSB") as! previewViewController
      
            vc.post = self.post
            vc.images = self.images
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }

    @IBAction func uploadPressed(_ sender: Any) {
        let optionMenu = UIAlertController(title: nil, message: "Choose Photo Source", preferredStyle: UIAlertControllerStyle.actionSheet)
        let photoLibrary = UIAlertAction(title: "Photo Library", style: UIAlertActionStyle.default, handler: { (alert: UIAlertAction!) -> Void in
            print("from library")
            //self.uploadButton.isHidden = true
            self.present(self.picker, animated: true, completion: nil)
        })
        let cameraOption = UIAlertAction(title: "Take a photo", style: UIAlertActionStyle.default, handler: {(alert: UIAlertAction!) -> Void in
            print("take a photo")
            //show camera
            self.picker.allowsEditing = true
            self.picker.sourceType = .camera
            self.picker.modalPresentationStyle = .popover
            //self.uploadButton.isHidden = true
            self.present(self.picker, animated: true, completion: nil)
        })
        let cancelOption = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: {(alert: UIAlertAction!) -> Void in
            print("Cancel")
            optionMenu.dismiss(animated: true, completion: nil)
        })
        optionMenu.addAction(photoLibrary)
        optionMenu.addAction(cancelOption)
        if UIImagePickerController.isSourceTypeAvailable(.camera) == true{
            optionMenu.addAction(cameraOption)
        }else{
            print("I dont have a camera")
        }
        
        present(optionMenu, animated: true, completion: nil)
    }
    
}
extension addImagesViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    internal func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        
        if let pickedImage = info[UIImagePickerControllerOriginalImage] as? UIImage{
            
            self.imageView.contentMode = .scaleAspectFill
            self.imageView.isHidden = false
            self.images.append(pickedImage)
            self.count += 1
            
            if self.count == 1 {
                self.countUploaded.text = "\(self.count)"
                self.imageView.image = pickedImage
            }else if self.count == 2{
                self.imageView.alpha = 0.3
                self.countUploaded.text = "\(self.count)"
                self.imageView2.image = pickedImage
                self.imageView2.isHidden = false
                self.imageView.isOpaque = true
            }else if self.count > 2 {
                self.countUploaded.text = "\(self.count)"
                self.imageView2.image = pickedImage
                self.imageView.image = self.images[self.count - 1]
            }
            self.view.setNeedsDisplay()
            
        }
        picker.dismiss(animated: true, completion: nil)
    }
    
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        picker.dismiss(animated: true, completion: nil)
        
    }
    
    
    
}
/*let optionMenu = UIAlertController(title: nil, message: "Choose Photo Source", preferredStyle: UIAlertControllerStyle.actionSheet)
let photoLibrary = UIAlertAction(title: "Photo Library", style: UIAlertActionStyle.default, handler: { (alert: UIAlertAction!) -> Void in
    print("from library")
    self.uploadButton.isHidden = true
    self.present(self.picker, animated: true, completion: nil)
})
let cameraOption = UIAlertAction(title: "Take a photo", style: UIAlertActionStyle.default, handler: {(alert: UIAlertAction!) -> Void in
    print("take a photo")
    //show camera
    self.picker.allowsEditing = true
    self.picker.sourceType = .camera
    self.picker.modalPresentationStyle = .popover
    self.uploadButton.isHidden = true
    self.present(self.picker, animated: true, completion: nil)
})
let cancelOption = UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: {(alert: UIAlertAction!) -> Void in
    print("Cancel")
    optionMenu.dismiss(animated: true, completion: nil)
})
optionMenu.addAction(photoLibrary)
optionMenu.addAction(cancelOption)
if UIImagePickerController.isSourceTypeAvailable(.camera) == true{
    optionMenu.addAction(cameraOption)
}else{
    print("I dont have a camera")
}

present(optionMenu, animated: true, completion: nil)*/
