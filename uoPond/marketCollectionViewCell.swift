//
//  marketCollectionViewCell.swift
//  uoPond
//
//  Created by Ethan Richards on 2/19/20.
//  Copyright © 2020 Ethan Richards. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseStorage
import FirebaseAuth


class marketCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var currentImage: UILabel!
    @IBOutlet weak var totalImages: UILabel!
    @IBOutlet weak var picNaviView: UIView!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var viewTag: UIView!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var userImage: UIButton!
    @IBOutlet weak var messageButton: UIButton!
    @IBOutlet weak var actualUserImage: UIImageView!
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var descripBox: UILabel!
    @IBOutlet weak var pinButton: UIButton!
    var uid = Auth.auth().currentUser?.uid
    var postID:String!
    var images = [UIImage]()
    var imageIndex = 0
    var currentImageCount = 1
    
    override func awakeFromNib() {
        super.awakeFromNib()
        //print("image count", images.count)
        messageButton.layer.cornerRadius = self.messageButton.frame.size.width / 2
        self.priceLabel.adjustsFontSizeToFitWidth = true
        //messageButton.layer.borderColor = UIColor.black.cgColor
        //messageButton.layer.borderWidth = 2
        viewTag.frame.size.height = 0
        //viewTag.layer.cornerRadius = 10
        viewTag.backgroundColor = UIColor.black.withAlphaComponent(0.40)
        self.setUpImage()
        //viewTag.layer.borderWidth = 2
        self.actualUserImage.layer.cornerRadius = self.userImage.frame.size.width / 2
        self.actualUserImage.clipsToBounds = true
        self.viewTag.heightAnchor.constraint(equalToConstant: 40).isActive = true
        self.descripBox.numberOfLines = 5
        self.descripBox.backgroundColor = UIColor.black.withAlphaComponent(0.40)
        //self.descripBox.sizeToFit()
        self.descripBox.lineBreakMode = NSLineBreakMode.byTruncatingTail
        self.titleLabel.adjustsFontSizeToFitWidth = true
        self.pinButton.layer.cornerRadius = self.pinButton.frame.size.width / 2
        self.totalImages.text = "\(self.images.count)"
        self.currentImage.text = "\(self.currentImageCount)"
        self.currentImage.adjustsFontSizeToFitWidth = true
        // set up view later .. work on stripe its 3AM.. lol 
        self.picNaviView.isHidden = true
    }
    
    @available(iOS 13.0, *)
    @IBAction func pinTapped(_ sender: Any) {
        if self.pinButton.imageView?.image == UIImage(systemName: "pin"){
            self.pinButton.setImage(UIImage(systemName: "pin.fill"), for: .normal)
            self.addToPins()
        }else{
             self.pinButton.setImage(UIImage(systemName: "pin"), for: .normal)
            self.removeFromPins()
        }
    }
    
    func addToPins(){
        let post = UUID().uuidString
        let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Posts")
        ref.child("Post\(self.postID!)").child("userPins").updateChildValues(["user\(post)":self.uid!])
        
    }
    
   
    
    func removeFromPins(){
        
    }
    
     func getFormattedCurrency(number: Int) -> String {
         let currencyFormatter = NumberFormatter()
         currencyFormatter.usesGroupingSeparator = true
         currencyFormatter.numberStyle = NumberFormatter.Style.currency
         
         currencyFormatter.locale = NSLocale.current
         let priceString = currencyFormatter.string(from: number as NSNumber)
         return priceString!
           
       }
    
    func setUpImage(){
        
        if images.count > 0 {
            postImage.image = self.images[0]
        }
        
        postImage.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped(sender:)))
        postImage.addGestureRecognizer(tap)
        postImage.isMultipleTouchEnabled = true
        let swipeUp = UISwipeGestureRecognizer(target: self, action: #selector(rotateImage(gesture:)))
        swipeUp.direction = .up
        let swipeDown = UISwipeGestureRecognizer(target: self, action: #selector(rotateImage(gesture:)))
        swipeDown.direction = .down
        self.postImage.addGestureRecognizer(swipeUp)
        self.postImage.addGestureRecognizer(swipeDown)
    }
    
     @objc func imageTapped(sender: UITapGestureRecognizer){
         if viewTag.isHidden == true{
             viewTag.isHidden = false
             messageButton.isHidden = false
            descripBox.isHidden = false
            pinButton.isHidden = false
         }else{
             viewTag.isHidden = true
             messageButton.isHidden = true
            descripBox.isHidden = true
            pinButton.isHidden = true
         }
     }
    
    
    @objc
       func rotateImage(gesture: UIGestureRecognizer){
           print("swipe")
           if let swipeGesture = gesture as? UISwipeGestureRecognizer{
           switch swipeGesture.direction{
           case UISwipeGestureRecognizerDirection.up:
               print("User swiped up")

               if imageIndex < self.images.count - 1 {
                   imageIndex += 1
                    self.currentImage.text = String(describing: self.currentImageCount -= 1 )
               }

               if imageIndex < self.images.count  {
                   self.postImage.image = self.images[self.imageIndex]
               }
           case UISwipeGestureRecognizerDirection.down:
               print("User swiped down")

               if imageIndex > 0{
                   imageIndex -= 1
                self.currentImage.text = String(describing: self.currentImageCount += 1 )

               }
               if imageIndex >= 0{
                   postImage.image = self.images[imageIndex]
                //self.currentImage.text = String(describing: self.currentImageCount -= 1)

                   self.postImage.setNeedsDisplay()
               }
              // if curretnImageIndex

               default:
                   break
               }
           }
       }
    
    
}
