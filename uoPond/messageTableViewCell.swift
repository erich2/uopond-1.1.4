//
//  messageTableViewCell.swift
//  uoPond
//
//  Created by Ethan Richards on 1/29/20.
//  Copyright © 2020 Ethan Richards. All rights reserved.
//

import UIKit

class messageTableViewCell: UITableViewCell {

    var textMess:String!
    var mine:Bool!
    var transaction:Bool!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.01) {
            self.setUpMess()
            print("set up : ", self.textMess)
        }
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
        let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
        label.numberOfLines = 0
        label.lineBreakMode = NSLineBreakMode.byWordWrapping
        label.font = font
        label.text = text

        label.sizeToFit()
        return label.frame.height
    }
    
    func widthForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
           let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
           label.numberOfLines = 0
           label.lineBreakMode = NSLineBreakMode.byWordWrapping
           label.font = font
           label.text = text

           label.sizeToFit()
           return label.frame.width
       }
    
    
    
    func setUpMess(){
        if self.transaction == true {
            let messLabel = UITextView(frame: CGRect(x: UIScreen.main.bounds.width/2 - 125, y: 0, width: 250, height: 20))
            messLabel.adjustsFontForContentSizeCategory = true
            messLabel.layer.cornerRadius = 15
            messLabel.isEditable = false
            messLabel.isSelectable = false
            messLabel.text = self.textMess!
            messLabel.layer.cornerRadius = 10
            messLabel.textColor = UIColor.lightGray
            messLabel.backgroundColor = UIColor.clear
            messLabel.font = UIFont.systemFont(ofSize: 12)
            messLabel.textAlignment = .center
            self.addSubview(messLabel)
            messLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
            messLabel.centerXAnchor.constraint(equalTo: self.contentView.centerXAnchor).isActive = true
        }else{
            if self.mine == true{
                let messLabel = UITextView(frame: CGRect(x: (UIScreen.main.bounds.width - widthForView(text: self.textMess, font: UIFont.systemFont(ofSize: 17), width: 250) - 20), y: 0, width: 10 + widthForView(text: self.textMess, font: UIFont.systemFont(ofSize: 17), width: 250), height: 20 + heightForView(text: self.textMess, font: UIFont.systemFont(ofSize: 17), width: 250)))
                
                
                messLabel.layer.cornerRadius = 15
                messLabel.isEditable = false
                messLabel.isSelectable = false
                
                messLabel.text = self.textMess!
                messLabel.layer.cornerRadius = 10
                if self.transaction == false {
                    messLabel.textColor = UIColor.white
                    messLabel.backgroundColor = UIColor.systemBlue
                    messLabel.font = UIFont.systemFont(ofSize: 17)
                    messLabel.textAlignment = .left
                }else if self.transaction == true {
                    messLabel.textColor = UIColor.lightGray
                    messLabel.backgroundColor = UIColor.clear
                    messLabel.font = UIFont.systemFont(ofSize: 12)
                    messLabel.textAlignment = .center
                }
                self.addSubview(messLabel)
                messLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
            }else if self.mine == false{
                     let messLabel = UITextView(frame: CGRect(x: 10, y: 0, width: 20 + widthForView(text: self.textMess, font: UIFont.systemFont(ofSize: 17), width: 250), height: 20 + heightForView(text: self.textMess, font: UIFont.systemFont(ofSize: 17), width: 250)))
                    messLabel.textColor = UIColor.white
                    messLabel.backgroundColor = UIColor.systemGray
                    messLabel.font = UIFont.systemFont(ofSize: 17)
                    messLabel.textAlignment = .left
                    messLabel.layer.cornerRadius = 15
                    messLabel.isEditable = false
                    messLabel.isSelectable = false
                    messLabel.text = self.textMess!
                    messLabel.layer.cornerRadius = 10
                    self.addSubview(messLabel)
                    messLabel.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
                
            }
        }
    }

}
