//
//  marketBiggerTableViewController.swift
//  uoPond
//
//  Created by Ethan Richards on 2/4/20.
//  Copyright © 2020 Ethan Richards. All rights reserved.
//

import UIKit
import Firebase
import FirebaseDatabase
import FirebaseStorage
import FirebaseAuth

class marketBiggerTableViewController: UITableViewController {

    var data = [String]()
    //var refresh: UIRefreshControl!
    var marketPosts = [Post]()
    var marketImage = [UIImage]()
    var userTitle = [String]()
    var uid = [String]()
    var myID:String?
    var myName:String?
    var profImage:UIImage?
    var clickedPath: IndexPath?
    var blacklist = [String]()
    var myUserName:String?
    var posts = [sectionPost]()
    var postID = [String]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.separatorColor = UIColor.black
        // add the logo into the nav bar
//        let logoView = UIImageView(image: UIImage(named: "navImage"))
//
//        logoView.contentMode = .scaleAspectFit
//        self.navigationItem.titleView = logoView
        self.data = ["furniture", "notes", "tech", "clothing", "books", "art"]
        self.tableView.backgroundColor = UIColor.white
        //let refresh = UIRefreshControl()
        //refresh.tintColor = UIColor.black
        //tableView.addSubview(refresh)
        addNavBarImage()
        getToolbar()
        setUpNavBar()
        DispatchQueue.global(qos: .background).async {
            self.gatherPosts(completion: {
                self.tableView.reloadData()
                self.tableView.setNeedsDisplay()    
            })
        }

    }
 
//    override func viewDidAppear(_ animated: Bool) {
//        self.data = []
//        DispatchQueue.main.asyncAfter(deadline: .now()+0.2) {
//            self.data = ["furniture", "notes", "tech", "clothing", "books", "art"]
//            self.tableView.reloadData()
//        }
//        self.navigationController?.navigationBar.tintColor = UIColor.black
//
//    }

    func addNavBarImage() {

        let navController = navigationController!

        let image = UIImage(named: "navImage") //Your logo url here
        let imageView = UIImageView(image: image)

        let bannerWidth = navController.navigationBar.frame.size.width
        let bannerHeight = navController.navigationBar.frame.size.height

        let bannerX = bannerWidth / 2 - (image?.size.width)! / 2
        let bannerY = bannerHeight / 2 - (image?.size.height)! / 2

        imageView.frame = CGRect(x: bannerX, y: bannerY, width: bannerWidth, height: bannerHeight)
        imageView.contentMode = .scaleAspectFit

        navigationItem.titleView = imageView
    }
    
    func setUpNavBar(){
        self.navigationController?.navigationBar.backgroundColor = UIColor.black
        self.navigationController?.navigationBar.barTintColor = UIColor.black
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationItem.hidesBackButton = true
    }
    
    @objc func backPressed(sender: UIBarButtonItem){
        let VC = self.storyboard!.instantiateViewController(withIdentifier: "homeSB") as! homeViewController
        self.navigationController?.pushViewController(VC, animated: false)
    }
    
    func getToolbar(){
         self.navigationController?.isToolbarHidden = false
           if #available(iOS 13.0, *) {
            let profile = UIBarButtonItem(image: UIImage(systemName: "person.circle.fill"), style: .plain, target: self, action: #selector(backPressed(sender:)))
            let addPost = UIBarButtonItem(title: "+", style: .plain, target: self, action: #selector(writePost(_:)))
            addPost.setTitleTextAttributes([NSAttributedStringKey.font :UIFont.boldSystemFont(ofSize: 30)], for: .normal)
            let marketButton = UIBarButtonItem(image: UIImage(systemName: "globe"), style: .plain, target: self, action: nil)
            marketButton.tintColor = UIColor.systemGray
               let rightSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
               //let leftSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
               self.toolbarItems = [ profile, rightSpace,addPost, rightSpace, marketButton]
           } else {
               // Fallback on earlier versions
           }
       }
    
    
    @objc func writePost(_ sender: Any) {
        let VC = self.storyboard!.instantiateViewController(withIdentifier: "popUpSB") as! popUpViewController
        VC.ownUserName = [MyVariables.userName]
        VC.fromMarket = true
        self.navigationController?.pushViewController(VC, animated: true)
        
    }
    
  
    
    // MARK: - Table view data source

    override func numberOfSections(in tableView: UITableView) -> Int {
        // #warning Incomplete implementation, return the number of sections
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        // #warning Incomplete implementation, return the number of rows
        return self.data.count
    }

    
    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "sectionCell", for: indexPath) as! sectionTableViewCell
        cell.categoryLabel.text = self.data[indexPath.row]
        cell.blacklist = self.blacklist
        if self.posts.count > 0 {
            for post in 0..<self.posts.count - 1 {
                if self.posts[post].type == self.data[indexPath.row] {
                    cell.correctPosts.append(self.posts[post])
                    cell.postID.append(self.postID[post])
                    cell.collectionView.setNeedsDisplay()
                }
            }
            //cell.totalItems.text = "(\(cell.correctPosts.count))"
        }
        
        
         if #available(iOS 13.0, *) {
            if self.data[indexPath.row] == "furniture"{
                cell.categoryEmote.tintColor = UIColor.systemRed.withAlphaComponent(0.90)
                cell.categoryEmote.image = UIImage(systemName: "bed.double")
            }
            if self.data[indexPath.row] == "notes"{
                cell.categoryEmote.tintColor = UIColor.systemTeal.withAlphaComponent(0.90)
                cell.categoryEmote.image = UIImage(systemName: "square.and.pencil")
              
            }
            if self.data[indexPath.row] == "tech"{
                cell.categoryEmote.tintColor = UIColor.systemGreen.withAlphaComponent(0.9)
                cell.categoryEmote.image = UIImage(systemName: "desktopcomputer")
               
            }
            if self.data[indexPath.row] == "clothing"{
                cell.categoryEmote.tintColor = UIColor.systemPurple.withAlphaComponent(0.9)
                cell.categoryEmote.image = UIImage(systemName: "flame.fill")
               
            }
            if self.data[indexPath.row] == "books"{
                cell.categoryEmote.tintColor = UIColor.systemIndigo.withAlphaComponent(0.90)
                cell.categoryEmote.image = UIImage(systemName: "book.fill")
               
            }
            if self.data[indexPath.row] == "art"{
                cell.categoryEmote.tintColor = UIColor.orange.withAlphaComponent(0.90)
                cell.categoryEmote.image = UIImage(systemName: "paintbrush.fill")
               
            }
        } else {
            //earlier version...
        }

     
        return cell
    }
    
    
    func gatherPosts(completion: @escaping () -> Void){
             let ref = Database.database().reference(fromURL: "https://uopond.firebaseio.com/Posts")
             ref.observe(.value) { (snapshot) in
                 if let snapshots = snapshot.children.allObjects as? [DataSnapshot]{
                     for child in snapshots{
                         let value = child.value as? NSDictionary
                         let category = value?["section"] as? String ?? ""
                         let downloadURL = value?["image0"] as? String ?? ""
                         let id = value?["user"] as? String ?? ""
                         let flagged = value?["flagged"] as? String ?? ""
                         let price = value?["price"] as? String ?? ""
                         let post = sectionPost(image: downloadURL, type: category, id: id, price: price)
                         
                         if (self.blacklist.contains(id)){
                             print("blocked user")
                         }else {
                             if (flagged == "yes"){
                                 print("flagged post")
                             }else{
                                 self.posts += [post]
                                 self.postID.append(id)
                                print(self.posts)
                             }
                         }
                         
                     }
                     completion()
                     }
                 else{
                     print("ERROR")
                 }
                 }
         }
  
    

    override func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let VC = self.storyboard!.instantiateViewController(withIdentifier: "newMarketSB") as! newMarketViewController
        //let VC = self.storyboard!.instantiateViewController(withIdentifier: "marketSB") as! marketTableViewController
        self.navigationController?.pushViewController(VC, animated: true)
        VC.myID = self.myID
        VC.myName = self.myName
        VC.myUserName = self.myUserName
        VC.navigationItem.title = self.data[indexPath.row]
        VC.blacklist = self.blacklist
        if #available(iOS 13.0, *) {
            if self.data[indexPath.row] == "furniture"{
                VC.color = UIColor.systemRed.withAlphaComponent(0.90)
                VC.emote = UIImage(systemName: "bed.double")
            }
            if self.data[indexPath.row] == "notes"{
               VC.color = UIColor.systemTeal.withAlphaComponent(0.90)
                VC.emote = UIImage(systemName: "square.and.pencil")
            }
            if self.data[indexPath.row] == "tech"{
                VC.color = UIColor.systemGreen.withAlphaComponent(0.9)
                VC.emote = UIImage(systemName: "desktopcomputer")
            }
            if self.data[indexPath.row] == "clothing"{
                VC.color = UIColor.systemPurple.withAlphaComponent(0.9)
                VC.emote = UIImage(systemName: "flame.fill")
            }
            if self.data[indexPath.row] == "books"{
                VC.color = UIColor.systemIndigo.withAlphaComponent(0.90)
                VC.emote = UIImage(systemName: "book.fill")
            }
            if self.data[indexPath.row] == "art"{
                VC.color = UIColor.orange.withAlphaComponent(0.90)
                VC.emote = UIImage(systemName: "paintbrush.fill")
            }
        }
   
    }

    
    @objc func sendView(sender:Any?){
        let button = sender as! UIButton
        let path = button.tag
        
        
        let VC = self.storyboard!.instantiateViewController(withIdentifier: "marketSB") as! marketTableViewController
        self.navigationController?.pushViewController(VC, animated: true)
        VC.myID = self.myID
        VC.myName = self.myName
    
        VC.navigationItem.title = self.data[path]
    }
    
    /*
    // Override to support conditional editing of the table view.
    override func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the specified item to be editable.
        return true
    }
    */

    /*
    // Override to support editing the table view.
    override func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            // Delete the row from the data source
            tableView.deleteRows(at: [indexPath], with: .fade)
        } else if editingStyle == .insert {
            // Create a new instance of the appropriate class, insert it into the array, and add a new row to the table view
        }    
    }
    */

    /*
    // Override to support rearranging the table view.
    override func tableView(_ tableView: UITableView, moveRowAt fromIndexPath: IndexPath, to: IndexPath) {

    }
    */

    /*
    // Override to support conditional rearranging of the table view.
    override func tableView(_ tableView: UITableView, canMoveRowAt indexPath: IndexPath) -> Bool {
        // Return false if you do not want the item to be re-orderable.
        return true
    }
    */

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
