//
//  homeCollectionViewCell.swift
//  uoPond
//
//  Created by Ethan Richards on 3/4/20.
//  Copyright © 2020 Ethan Richards. All rights reserved.
//

import UIKit

class homeCollectionViewCell: UICollectionViewCell {
    
    
    @IBOutlet weak var thumbNail: UIImageView!
    
    @IBOutlet weak var view: UIView!
    
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var categoryEmote: UIButton!
    @IBOutlet weak var expiration: UILabel!
    
    override func awakeFromNib() {
        self.layer.cornerRadius = 10
        self.categoryEmote.layer.cornerRadius = self.categoryEmote.frame.size.width / 2
        self.layer.borderColor = UIColor.black.cgColor
        self.layer.borderWidth = 1
        self.thumbNail.contentMode = .scaleAspectFill
    }
    
}
