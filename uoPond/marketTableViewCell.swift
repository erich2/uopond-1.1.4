//
//  marketTableViewCell.swift
//  uoPond
//
//  Created by Ethan Richards on 5/21/19.
//  Copyright © 2019 Ethan Richards. All rights reserved.
//

import UIKit

class marketTableViewCell: UITableViewCell {
    
    
    
    
    @IBOutlet weak var descripBox: UILabel!
    @IBOutlet weak var descripToggle: UIButton!
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var postImage: UIImageView!
    @IBOutlet weak var userImage: UIButton!
    @IBOutlet weak var actualUserImage: UIImageView!
    @IBOutlet weak var messageButton: UIButton!
    @IBOutlet weak var viewTag: UIView!
    var images = [UIImage]()
    
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        postImage.isUserInteractionEnabled = true
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.imageTapped(sender:)))
        postImage.addGestureRecognizer(tap)
        messageButton.layer.cornerRadius = self.messageButton.frame.size.width / 2
        
        self.priceLabel.adjustsFontSizeToFitWidth = true
        //messageButton.layer.borderColor = UIColor.black.cgColor
        //messageButton.layer.borderWidth = 2
        viewTag.frame.size.height = 0
        //viewTag.layer.cornerRadius = 10
        viewTag.backgroundColor = UIColor.black.withAlphaComponent(0.40)
        
        //viewTag.layer.borderWidth = 2
        self.postImage.image = self.images[0]
        self.actualUserImage.layer.cornerRadius = self.userImage.frame.size.width / 2
        self.actualUserImage.clipsToBounds = true
        self.viewTag.heightAnchor.constraint(equalToConstant: 40).isActive = true
        self.descripBox.isHidden = true
        self.descripBox.numberOfLines = 5
        self.descripBox.backgroundColor = UIColor.black.withAlphaComponent(0.40)
        //self.descripBox.sizeToFit()
        self.descripBox.lineBreakMode = NSLineBreakMode.byTruncatingTail
        self.descripBox.frame.size.width = 375
        self.titleLabel.adjustsFontSizeToFitWidth = true
    }
    
   
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        
    }
    
    
    
    @available(iOS 13.0, *)
    @IBAction func descripToggled(_ sender: Any) {
        if self.descripBox.isHidden == true{
            self.descripBox.isHidden = false
            self.descripToggle.setImage(UIImage(systemName: "chevron.up"), for: .normal)
            
        }else{
            self.descripBox.isHidden = true
            self.descripToggle.setImage(UIImage(systemName: "chevron.down"), for: .normal)

        }
    }
    
    func getFormattedCurrency(number: Int) -> String {
        let currencyFormatter = NumberFormatter()
        currencyFormatter.usesGroupingSeparator = true
        currencyFormatter.numberStyle = NumberFormatter.Style.currency
        
        currencyFormatter.locale = NSLocale.current
        let priceString = currencyFormatter.string(from: number as NSNumber)
        return priceString!
          
      }
   
    @objc func imageTapped(sender: UITapGestureRecognizer){
        if viewTag.isHidden == true{
            viewTag.isHidden = false
            messageButton.isHidden = false
        }else{
            viewTag.isHidden = true
            messageButton.isHidden = true
        }
    }

    

    
}
