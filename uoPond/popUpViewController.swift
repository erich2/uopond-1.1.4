//
//  popUpViewController.swift
//  uoPond
//
//  Created by Ethan Richards on 4/30/19.
//  Copyright © 2019 Ethan Richards. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth
import FirebaseDatabase
import FirebaseStorage

class popUpViewController: UIViewController, UITextFieldDelegate, UITextViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate {

   
   
    @IBOutlet weak var descripHeight: NSLayoutConstraint!
    //@IBOutlet weak var descripHeight: NSLayoutConstraint!
    @IBOutlet weak var priceCheck: UIImageView!
    @IBOutlet weak var category: UIPickerView!
    
    @IBOutlet weak var sectionCheck: UIImageView!
    @IBOutlet weak var descripCheck: UIImageView!
    @IBOutlet weak var titleCheck: UIImageView!
    @IBOutlet weak var titleBox: UITextField!
    
    @IBOutlet weak var descriptionBox: UITextView!
    //@IBOutlet weak var descriptionBox: UITextField!
    
    @IBOutlet weak var priceBox: UITextField!
    var fromMarket:Bool = false

    
    @IBOutlet weak var addButton: UIButton!
    var ref:DatabaseReference?
    
    var ownUserName = [String]()
    var data = [String]()
    var section: String!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.data = ["categories:","furniture", "notes", "tech", "clothing", "books", "art"]
        self.setUpPicker()
        self.showAnimate()
        setUpView()
        addDoneButton()
        if #available(iOS 13.0, *) {
            addToolbarItems()
        } else {
            // Fallback on earlier versions
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        self.navigationController?.toolbar.barTintColor = UIColor.black
    }
    
    func setUpView(){
        self.roundCorners(view: self.titleBox, corners: [.topLeft, .topRight], radius: 10)
        self.roundCorners(view: self.priceBox, corners: [.bottomRight, .bottomLeft], radius: 10)
        self.titleBox.layer.borderColor = UIColor.black.cgColor
        self.titleBox.layer.borderWidth = 1
        self.descriptionBox.layer.borderColor = UIColor.black.cgColor
        self.descriptionBox.layer.borderWidth = 1
        self.priceBox.layer.borderColor = UIColor.black.cgColor
        self.priceBox.layer.borderWidth = 1
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapped(sender:)))
        self.view.addGestureRecognizer(tap)
    
        self.priceBox.attributedPlaceholder = NSAttributedString(string: "price", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        self.titleBox.attributedPlaceholder = NSAttributedString(string: "title", attributes: [NSAttributedString.Key.foregroundColor: UIColor.lightGray])
        self.navigationController?.isToolbarHidden = false
        self.navigationController?.toolbar.barTintColor = UIColor.black
        titleBox.delegate = self
        self.navigationController?.navigationBar.barTintColor = UIColor.black
        descriptionBox.delegate = self
        priceBox.delegate = self
        self.navigationItem.title = "New Listing"
        self.navigationItem.titleView?.tintColor = UIColor.white
        self.navigationController?.toolbar.barTintColor = UIColor.black
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white]
        self.descripHeight.constant = self.heightForView(text: "F", font: UIFont.systemFont(ofSize: 16), width: 320) + 20
        self.descriptionBox.text = "quick description"
        self.descriptionBox.textColor = UIColor.lightGray
    }
    
    @available(iOS 13.0, *)
    func addToolbarItems(){
        let dismiss = UIBarButtonItem(title: "Cancel", style: .plain, target: self, action: #selector(dismissPressed(_:)))
        
        let next = UIBarButtonItem(title: "Next", style: .plain, target: self, action: #selector(previewPressed))
        let middleSpace = UIBarButtonItem(barButtonSystemItem: .flexibleSpace, target: self, action: nil)
        if self.fromMarket == true{
            self.toolbarItems = [middleSpace, next]
        }else {
            self.toolbarItems = [dismiss, middleSpace, next]
        }
    }
    
    func addDoneButton(){
        let keyboardToolBar = UIToolbar()
        keyboardToolBar.sizeToFit()
         
        let flexibleSpace = UIBarButtonItem(barButtonSystemItem:
            UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let doneButton = UIBarButtonItem(barButtonSystemItem:
            UIBarButtonSystemItem.done, target: self, action: #selector(self.doneClicked) )
         
        keyboardToolBar.setItems([flexibleSpace, doneButton], animated: true)
         
        descriptionBox.inputAccessoryView = keyboardToolBar
        titleBox.inputAccessoryView = keyboardToolBar
        priceBox.inputAccessoryView = keyboardToolBar
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
           return self.data.count
       }
    
    func setUpPicker(){
        self.category.dataSource = self
        self.category.delegate = self
    }
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        var attributedString: NSAttributedString!
        attributedString = NSAttributedString(string: self.data[row], attributes: [NSAttributedStringKey.foregroundColor : UIColor.white, NSAttributedStringKey.font : UIFont(name: "Symbol", size: 10)!])
        //attributedString = NSAttributedString(string: self.data[row], attributes: [NSAttributedStringKey.font : UIFont(name: "Symbol", size: 13)!])

        return attributedString
       }

    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
   
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
           self.section = self.data[row]
        if self.data[row] != "categories:" {
            self.sectionCheck.isHidden = false
        }else {
            self.sectionCheck.isHidden = true

        }
       }
    
    @objc func doneClicked(){
        view.endEditing(true)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if self.descriptionBox.text?.count == 0 {
            self.descripHeight.constant = self.heightForView(text: "F", font: UIFont.systemFont(ofSize: 16), width: 320) + 20
        }else if self.descripHeight.constant <= 55 {
            self.descripHeight.constant = self.heightForView(text: self.descriptionBox.text!, font: UIFont.systemFont(ofSize: 16), width: 320) + 20
        }
        if (self.descriptionBox.text?.count)! > 10{
            self.descripCheck.isHidden = false
        }else{
            self.descripCheck.isHidden = true
        }
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if textView.textColor == UIColor.lightGray {
            textView.text = nil
            textView.textColor = UIColor.white
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if textView.text.isEmpty {
            textView.text = "quick description"
            textView.textColor = UIColor.lightGray
        }
    }
    
    @IBAction func priceValidate(_ sender: Any) {
        if (self.priceBox.text?.count)! > 0{
            self.priceCheck.isHidden = false
            
        }else{
            self.priceCheck.isHidden = true
        }
    }
    
    func heightForView(text:String, font:UIFont, width:CGFloat) -> CGFloat{
              let label:UILabel = UILabel(frame: CGRect(x: 0, y: 0, width: width, height: CGFloat.greatestFiniteMagnitude))
              label.numberOfLines = 0
              label.lineBreakMode = NSLineBreakMode.byWordWrapping
              label.font = font
              label.text = text

              label.sizeToFit()
              return label.frame.height
          }
  
    
    @IBAction func titleValidate(_ sender: Any) {
        if (self.titleBox.text?.count)! > 3{
            self.titleCheck.isHidden = false
        }else{
            self.titleCheck.isHidden = true
        }
    }
    
    @objc func tapped(sender: UIGestureRecognizer) {
        self.view.endEditing(true)
        print("tapped")
    }
    
    func roundCorners(view :UIView, corners: UIRectCorner, radius: CGFloat){
            let path = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
            let mask = CAShapeLayer()
            mask.path = path.cgPath
            view.layer.mask = mask
    }
    
 
    
   
    @objc func previewPressed(_sender:Any){
        if self.titleCheck.isHidden == true && self.priceCheck.isHidden == true && self.descripCheck.isHidden == true && self.sectionCheck.isHidden == true {
            let alertController = UIAlertController(title: "Oops", message: "Please fill out all fields", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            self.present(alertController, animated: true, completion: nil)
        }else{
            let vc = self.storyboard!.instantiateViewController(withIdentifier: "addImageSB") as! addImagesViewController
            let price1 = getFormattedCurrency(number: Int(self.priceBox.text!)!)
        
            let post = tempPost(descrip: self.descriptionBox.text!, title: self.titleBox.text!, price: price1, userName: self.ownUserName[0], category: self.section)
            vc.post = post
            self.navigationController?.pushViewController(vc, animated: true)
            
        }
    }
    
  
    
    
    func getFormattedCurrency(number: Int) -> String {
      let currencyFormatter = NumberFormatter()
      currencyFormatter.usesGroupingSeparator = true
      currencyFormatter.numberStyle = NumberFormatter.Style.currency
      
      currencyFormatter.locale = NSLocale.current
      let priceString = currencyFormatter.string(from: number as NSNumber)
      return priceString!
        
    }

    
  
    
    @objc func dismissPressed(_ sender: Any) {
        self.performSegue(withIdentifier: "backToHome", sender: self)
    }
    
    
    func showAnimate(){
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25) {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        }
    }
    
    func dismissAnimate(){
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }) { (finished) in
            if (finished){
                self.view.removeFromSuperview()
            }
        }
    }
    
    
    
}


