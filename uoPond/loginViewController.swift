//
//  loginViewController.swift
//  uoPond
//
//  Created by Ethan Richards on 4/19/19.
//  Copyright © 2019 Ethan Richards. All rights reserved.
//

import UIKit
import Firebase
import FirebaseAuth

class loginViewController: UIViewController {

    
    @IBOutlet weak var successLogin: UIImageView!
    @IBOutlet weak var usernameBox: UITextField!
    
    @IBOutlet weak var passwordBox: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    
    @IBOutlet weak var loginBackView: UIView!
    
    @IBOutlet weak var toRegister: UIButton!
    
    @IBOutlet weak var shadowView: UIView!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(self.tapped(sender:)))
        self.view.addGestureRecognizer(tap)
        self.view.applyGradient(colours: [UIColor.black, UIColor.clear], locations: [0.0, 0.75, 1.0])
        self.roundCorners(view: self.usernameBox, corners: [.topLeft, .topRight], radius: 10)
        self.roundCorners(view: self.passwordBox, corners: [.bottomLeft, .bottomRight], radius: 10)
        self.usernameBox.addBottomBorder(UIColor.black.withAlphaComponent(0.4), height: 1)
        self.loginBackView.addShadowFinalLonger()
        self.shadowView.addShadowFinal()
        self.roundCorners(view: self.loginButton, corners: [.topRight, .topLeft], radius: 10)
        self.roundCorners(view: self.toRegister, corners: [.bottomRight, .bottomLeft], radius: 10)
        self.loginButton.addBottomBorder(UIColor.black.withAlphaComponent(0.4), height: 1)
        self.loginBackView.layer.cornerRadius = 10
    }
    
    func roundCorners(view :UIView, corners: UIRectCorner, radius: CGFloat){
             let path = UIBezierPath(roundedRect: view.bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
             let mask = CAShapeLayer()
             mask.path = path.cgPath
             view.layer.mask = mask
     }
        

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @objc func tapped(sender: UIGestureRecognizer) {
        self.view.endEditing(true)
        print("tapped")
    }
    
    @IBAction func toRegister(_ sender: Any) {
        let VC = self.storyboard!.instantiateViewController(withIdentifier: "registerSB") as! registerViewController
        self.navigationController?.pushViewController(VC, animated: false)
    }
    
    
    @IBAction func signInPressed(_ sender: Any) {
        if (passwordBox.text == "") || (usernameBox.text == ""){
            let alertController = UIAlertController(title: "Oops", message: "Please fill out all fields", preferredStyle: .alert)
            let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
            alertController.addAction(defaultAction)
            
            self.present(alertController, animated: true, completion: nil)
            
            
        }else{

            self.logIn()
            
        }
    }
    
 
    
    func logIn(){
        self.showSpinner(onView: self.loginBackView)
        self.loginButton.titleLabel?.textColor = UIColor.lightGray
        Auth.auth().signIn(withEmail: usernameBox.text!, password: passwordBox.text!) { (signin, error) in
            if error == nil{
                self.loginButton.isEnabled = false
                self.usernameBox.isHidden = true
                self.passwordBox.isHidden = true
                self.successLogin.isHidden = false
                self.removeSpinner()
                DispatchQueue.main.asyncAfter(deadline: .now() + 1) {
                    self.performSegue(withIdentifier: "loginToHome", sender: self)
                }
               
            }else{
                self.loginButton.isEnabled = true
                self.usernameBox.isHidden = false
                self.passwordBox.isHidden = false
                self.successLogin.isHidden = true
                self.removeSpinner()
                let alertController = UIAlertController(title: "Unable to Sign in.", message: error?.localizedDescription, preferredStyle: .alert)
                let defaultAction = UIAlertAction(title: "OK", style: .cancel, handler: nil)
                alertController.addAction(defaultAction)
                self.present(alertController, animated: true, completion: nil)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "loginToHome"{
            let vc = segue.destination as! UINavigationController
            let target = vc.topViewController as! homeViewController
            target.userUid = Auth.auth().currentUser?.uid
        }else if segue.identifier == "loginToMarket" {
            let vc = segue.destination as! UINavigationController
            let target = vc.topViewController as! marketTableViewController
            target.myID = Auth.auth().currentUser?.uid
        }
    }
    
    
    @IBAction func homePressed(_ sender: Any) {
        self.performSegue(withIdentifier: "loginToHome", sender: self)
    }
    
}



























